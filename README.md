# NEXT-RSV-PIPE


[[_TOC_]]

## 1. Introduction

`NEXT-RSV-PIPE` is an advanced solution designed for the efficient and accurate assembly of RSV genome sequences based on short read data. This workflow is tailored to handle the intricacies and challenges of RSV genome sequencing, ensuring high-quality and accurate results.

Key functionalities of the workflow include:

  a. **Automated Reference Selection** (optional): Provides a data-driven approach to selecting the most suitable reference from a curated set of RSV A and B reference genomes.
  
  b. **Adapter Clipping** (optional): Purges the data of adapter remnants, preserving the integrity of the genomic reads.
  
  c. **Primer Clipping** (optional): Ensures that only the genuine RSV genomic sequences are considered by removing any primer sequences.
  
  d. **Quality-Based Read Trimming**: Refines the dataset by excising low-quality bases, elevating the overall quality of the reads.
  
  e. **Read Decontamination** (optional): Implements stringent measures to eliminate non-RSV sequences, safeguarding against potential contaminants.
  
  f. **Read Mapping**: Aligns the processed reads to the chosen reference genome, forming the foundation for accurate assembly.
  
  g. **Read Deduplication**: Rectifies potential amplification biases by ensuring the uniqueness of reads.
  
  h. **Variant Calling**: Delivers a precise account of genomic variations, offering insights into strain-specific mutations and divergences.
  
  i. **Consensus Sequence Generation**: Constructs a unified genomic representation, encapsulating the dominant sequences within your sample.
  
  j. **Variant Annotation**: Predicts the effect of genomic variations, offering more detailed insights into strain-specific characteristics.
  
  k. **Statistical Assessment and Reporting**: Furnishes an in-depth evaluation of the assembly, elucidating the fidelity and robustness of the derived sequences.
  
## 2. Workflow Description

The pipeline is structured into distinct stages, each integrating specialized tools and offering customizable parameters to enhance flexibility and robustness.

### **a. Automated Reference Selection (Optional)**
This module automatically determines the most suitable reference genome by aligning raw sequencing reads against a curated database of **RSV A** and **RSV B** sequences using **KMA**.  
- The reference genome with the highest alignment score is selected.  
- This reference is subsequently used for read mapping, consensus generation, and consensus annotation.  
- A predefined reference can be defined by the user to skip this step.  

```bash
# Display genome accessions included in the curated built-in database.
path/to/next-rsv-seq/rsvpipe --show-refs
```

### **b. Adapter Clipping (Optional)**
Residual adapter sequences introduced during library preparation are removed from raw reads using **Trimmomatic** to prevent contamination of downstream analyses.  
- Uses an default or user defined file (fasta) containing the adapter sequences specific to the library preparation kit.  
- Improves mapping accuracy and reduces alignment artifacts.  

### **c. Quality-Based Read Trimming**
Low-quality bases are removed from sequencing reads using **Trimmomatic** (sliding window approach), ensuring high-confidence downstream analysis.  
- A **Phred quality score threshold** is applied to discard unreliable bases.  
- Short reads below a predefined length threshold are also removed to maintain data quality.  

### **d. Read Decontamination (Optional)**
If a **Kraken2** database is available, non-RSV sequences that may originate from contamination (e.g., host DNA, bacterial, or viral co-infections) are identified and filtered out.  
- Requires a pre-built Kraken2 database.  
- Improves specificity by ensuring only RSV-related reads are processed.  

### **e. Read Mapping**
Processed reads are aligned to the selected RSV reference genome using **bwa-mem2**, a high-performance alignment algorithm.  
- Outputs include alignment statistics, read depth distribution, and mapping quality assessments.  
- The resulting **BAM files** serve as the basis for further analysis.  

### **f. Primer Clipping (Optional)**
To avoid artificial variations introduced by primer sequences, **ivar trim** is used to remove primers from aligned reads.  
- Requires a primer BED file matching the reference genome.  
- Ensures accurate representation of viral genetic sequences by acvoiding variant dilution.  

### **g. Read Deduplication (Optional)**
Duplicate reads, often introduced due to PCR amplification biases, are identified and removed using **Picard MarkDuplicates**.  
- Improves variant calling accuracy by reducing artificial read depth inflation.  
- Retains a single representative read from each duplicate cluster.  

### **h. Variant Calling**
Genomic variants (e.g., SNPs, insertions, and deletions) in the RSV genome are detected using **ivar**.  
- Requires a minimum read depth and variant allele frequency threshold.   

### **i. Consensus Sequence Generation**
A consensus genome sequence is reconstructed from the mapped reads using **ivar**.  
- Positions with low read coverage may be masked by N to indicate uncertainty.  
- The consensus sequence represents the dominant viral population in the sample.  

### **j. Variant Annotation**
Genomic variants are functionally annotated using **SnpEff**, providing insights into their biological impact.  
- Identifies synonymous, missense, and nonsense mutations.  
- To harmonize annotations across different reference genomes and, thus, to ensure comparability across different datasets, detected variants are lifted over to:  
  - **LR699737** (*RSV A*)  
  - **OP975389** (*RSV B*)  
  
### **k. Statistical Assessment and Reporting**
Comprehensive reports summarizing sequencing metrics, alignment quality, and variant analysis are generated.  
- Custom summary tables provide an overview of key statistics including mapping statistics, alerts, and mutation of concern.  

## 3. Output Files and Result Interpretation

For each sample, the pipeline generates multiple output files containing essential results, including consensus sequences, variant annotations, quality metrics, and genome annotations.

### **3.3 Summary Statistics (`summary.tsv`)**
This **tab-separated values (TSV)** file aggregates per-sample quality control metrics, variant effects, and coverage statistics. Below is a breakdown of its columns:

| Column Name                                  | Description |
|----------------------------------------------|-------------|
| `sample`                                     | Sample identifier. |
| `alert`                                      | Overall alert status determined by the pattern and impact of detected genomic mutations. |
| `alert_level`                                | Overall alert level (higher values indicate greater criticality). |
| `moc_of_alert_level_5` .. `moc_of_alert_level_1`<sup>[1]</sup>                       | Detected mutations of concern, categorized by the distinct alert levels. |
| `missense_variant`<sup>[2]</sup>                          | Count of missense mutations, divided into **F-gene mutations** and **total mutations**. |
| `stop_gained`<sup>[2]</sup>                                | Number of premature stop codon mutations, divided into **F-gene mutations** and **total mutations**. |
| `conservative_inframe_insertion`<sup>[2]</sup>            | Number of in-frame insertions with conservative impact, divided into **F-gene mutations** and **total mutations**. |
| `disruptive_inframe_insertion`<sup>[2]</sup>              | Number of in-frame insertions with disruptive effects, divided into **F-gene mutations** and **total mutations**. |
| `conservative_inframe_deletion`<sup>[2]</sup>             | Number of in-frame deletions with conservative impact, divided into **F-gene mutations** and **total mutations**. |
| `disruptive_inframe_deletion`<sup>[2]</sup>               | Number of in-frame deletions with disruptive effects, divided into **F-gene mutations** and **total mutations**. |
| `frameshift_variant`<sup>[2]</sup>                        | Count of frameshift mutations, divided into **F-gene mutations** and **total mutations**. |
| `alert_reference`                            | Reference genome used for variant annotation and alert classification. |
| `mapping_reference`                          | Reference genome used for read mapping and genome annotation. |
| `raw_read_count`                             | Total number of raw reads before processing. |
| `trimmed_read_count`                         | Number of reads after quality trimming. |
| `mapped_read_count`                          | Number of reads successfully mapped to the reference genome. |
| `dedupped_mapped_read_count`                 | Number of unique reads after removing PCR duplicates. |
| `filtered_mapped_read_count`                 | Number of mapped reads after filtering. |
| `20x_covered_genome_fraction_in_bp`         | Number of base pairs covered at least 20-fold. |
| `20x_covered_genome_fraction_in_%`          | Percentage of the genome covered at least 20-fold. |
| `50x_covered_genome_fraction_in_bp`         | Number of base pairs covered at least 50-fold. |
| `50x_covered_genome_fraction_in_%`          | Percentage of the genome covered at least 50-fold. |
| `100x_covered_genome_fraction_in_bp`        | Number of base pairs covered at least 100-fold. |
| `100x_covered_genome_fraction_in_%`         | Percentage of the genome covered at least 100-fold. |
| `200x_covered_genome_fraction_in_bp`        | Number of base pairs covered at least 200-fold. |
| `200x_covered_genome_fraction_in_%`         | Percentage of the genome covered at least 200-fold. |
| `500x_covered_genome_fraction_in_bp`        | Number of base pairs covered at least 500-fold. |
| `500x_covered_genome_fraction_in_%`         | Percentage of the genome covered at least 500-fold. |
| `avg_sequencing_depth`                       | Average sequencing depth across the genome. |

<sup>[1]</sup> Only variants exceeding a specified threshold are counted (default: 0.05; adjustable via `--al`).  
<sup>[2]</sup> Only variants exceeding a specified threshold are counted (default: 0.5; adjustable via `--re`).

### **3.2 Per-Sample Output Files**
Each processed sample produces the following key output files:

- **Consensus Sequence (`results/{sample}.fasta`)**  
  - Contains the final consensus genome sequence after read mapping and variant calling.
  - Positions with insufficient coverage are masked with **N** to indicate uncertainty.

- **Variant Annotation (`results/{sample}_variant_annotation.tsv`)**  
  - Lists identified mutations, their genomic positions, and functional impacts. 
  - See section 3.2 for more details.

- **Quality Metrics (`results/{sample}_qual.txt`)**  
  - Provides PHRED-encoded base call qualities for the consensus sequence.

- **Genome Annotation (`results/{sample}.gff`)**  
  - Annotated genome structure based on the reference genome.

### **3.3 Variant Annotation File (`{sample}_variant_annotation.tsv`)**
This **tab-separated values (TSV)** file contains detailed information about identified genomic variants. Below is a description of each column:

| Column Name          | Description |
|----------------------|-------------|
| `SAMPLE`            | Sample identifier. |
| `MAPPING_REFERENCE` | Reference genome used for read mapping . |
| `MAPPING_POS`       | Position of the variant in the mapping reference. |
| `REFERENCE`         | Reference genome used for variant annotation (after lift-over). |
| `POS`               | Genomic position of the variant based on REFERENCE, empty if lift-over failed. |
| `REF`               | REFERENCE nucleotide(s) at the given position. |
| `ALT`               | Alternate nucleotide(s) (mutation observed). |
| `REF_DP`            | Depth of coverage for the REFERENCE allele. |
| `ALT_DP`            | Depth of coverage for the alternate allele. |
| `TOTAL_DP`          | Total sequencing depth at the position. |
| `ALT_FREQ`          | Allele frequency of the alternate variant. |
| `PVAL`              | Statistical significance of the variant call. |
| `PASS`              | Quality filter PASS status (True or False). |
| `Variant_Type`      | Type of variant (e.g., frameshift_variant). |
| `Effect`            | Predicted molecular effect (e.g., synonymous, missense). |
| `Impact`            | Severity of the variant (e.g., low, moderate, high). |
| `Gene`              | Gene affected by the variant. |
| `HGVS_c`            | Variant annotation in **HGVS** format (cDNA level). |
| `HGVS_p`            | Variant annotation in **HGVS** format (protein level). |
| `AA_Pos`            | Amino acid position of the mutation based on REFERENCE annotation. |
| `Region`            | Genomic region where the mutation occurs based on REFERENCE annotation (e.g., CDS, UTR). |

Please consider, that only variants exceeding a specified threshold are counted (default: 0.05; adjustable via `--al`).
Additionally, parameters for variant calling and consensus masking (see section 6.8 and 6.9) impact considered variants and consensus sites.


## 4. Setup 

### Prerequisites

Before deploying `NEXT-RSV-PIPE`, it's imperative to have both `conda` and `mamba` installed on the target system. These dependencies are crucial for ensuring the seamless operation of the tool.

#### Installing `conda`
If you haven't installed it yet, it's usually best to set up conda using the Miniconda or Anaconda distribution.

1. **Miniconda** (lightweight and recommended):
    ```bash
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    bash Miniconda3-latest-Linux-x86_64.sh
    ```

2. **Anaconda** (includes more packages by default):
    ```bash
    wget https://repo.anaconda.com/archive/Anaconda3-latest-Linux-x86_64.sh
    bash Anaconda3-latest-Linux-x86_64.sh
    ```

Remember to follow the on-screen instructions and initialize `conda` by sourcing your shell profile (e.g., `~/.bashrc` or `~/.zshrc`).

#### Installing `mamba`
Once `conda` is installed, installing `mamba` is straightforward:

```bash
conda install mamba -c conda-forge
```

### Installing `NEXT-RSV-PIPE`

To make `NEXT-RSV-PIPE` available, begin by cloning the repository directly to your current working directory:

```bash
git clone https://gitlab.com/rki_bioinformatics/next-rsv-seq
```

Upon completion, validate your installation by accessing its help page. Please note that the initial run takes longer since the program  will automatically fetch and install additional necessary dependencies:

```bash
next-rsv-seq/rsvpipe -h
```

With these steps, `NEXT-RSV-PIPE` is set up and operational.


## 5. Basic Usage

As a minimum the `NEXT-RSV-PIPE` needs gz-compressed paired-end fastq-files or a directory containing such fastq files as input. Ensure that the files are both gz-compressed and paired, as these are mandatory requirements to run the pipeline succesfully.

### Running the Pipeline

a. **Using direct file input**:
   Execute the standard workflow by directly specifying the gz-compressed paired-end fastq files as input:

   ```bash
   path/to/next-rsv-seq/rsvpipe -f sample_A_R1_L001.fastq.gz sample_A_R2_L001.fastq.gz
   ```

b. **Using a directory**:
   To employ a directory containing the necessary fastq files for the standard workflow, use:

   ```bash
   path/to/next-rsv-seq/rsvpipe -d fastq_dir
   ```

c. **Recursive directory search**:
   If you wish to search within a given directory recursively for all embedded fastq files, use:

   ```bash
   path/to/next-rsv-seq/rsvpipe -d fastq_dir -r
   ```

Ensure you replace `path/to/` with the actual path to your `NEXT-RSV-PIPE` installation (see section 2).


## 6. Options to Customize the Workflow

For a detailed overview of all options, refer to the help page:

```bash
path/to/next-rsv-seq/rsvpipe -h
```

### 6.1 Adjusting Reference Selection

By default, `NEXT-RSV-PIPE` utilizes KMA to select the most appropriate reference for each fastq dataset, based on a curated collection of RSV A and B reference genomes. 

For retrospective whole-genome sequencing (WGS) analyses of viruses collected around the onset of duplication events, it is recommended to verify the presence or absence of G gene duplication through prior de novo assembly. This applies to samples collected around 2010 for RSV-A and 1999 for RSV-B. During these periods, publicly available reference genomes were limited, and phylogenetically related strains with and without the duplication circulated simultaneously. This may introduce bias in selecting an appropriate reference sequence.

#### Changing the Fall-back Reference

In cases where KMA fails to identify a suitable reference, it defaults to KM5175731 as the fall-back reference. You can specify a different default reference using the `--fallback-ref` option.

```bash
# Setting JQ582844 as the fall-back reference
path/to/next-rsv-seq/rsvpipe --fallback-ref JQ582844 -d path/to/fastq_folder
```

To view a list of supported references, use the following command:

```bash
path/to/next-rsv-seq/rsvpipe --show-refs
```

### 6.2 Adding New Reference Sequences to the Collection

For advanced users who want to include more reference genomes, you can add new ones to the path/to/next-rsv-seq/data/references directory. Just make sure that for each genome you add, you include both a .fasta file with the sequence data and a .gff file with the genome features. Importantly, each CDS (coding sequence) in the .gff file should have a corresponding gene element.

#### Using User-defined Reference(s)

If you wish to designate a specific reference for all fastq datasets, specify the appropriate `.fasta` and `.gff` files using the `--ref` and `--gff` options, respectively.

```bash
# Using a custom reference JQ582844
path/to/next-rsv-seq/rsvpipe --ref path/to/JQ582844.fasta --gff path/to/JQ582844.gff -d path/to/fastq_folder
```

For those who need to use dataset-specific references, prepare a tab-delimited text file with a header row that includes the following columns:

    1. SAMPLE: The basic name of your dataset
    2. FASTA: The full path to your .fasta file containing the reference sequence
    3. GFF: The full path to your .gff file with the annotated reference details

Ensure each piece of information is clearly labeled and organized under its respective heading for straightforward reference integration.

```bash
# Using dataset-specific references 
path/to/next-rsv-seq/rsvpipe --ref-map path/to/references.txt -d path/to/fastq_folder
```

Note: Always replace `path/to/` placeholders with the specific paths applicable on your system.


### 6.3 Activating Adapter Clipping

To ensure the accuracy of alignments to the reference sequence, it's crucial to clip adapter sequences. For this operation, you will need to supply a FASTA file containing the desired adapter sequences using the `--adapter` option. If this option isn't used, the pipeline will skip adapter clipping.

#### Running Adapter Clipping

To initiate adapter clipping using specific adapter sequences found in a FASTA file, execute the following:

```bash
# Clipping adapter sequences specified in adapters.fasta
path/to/next-rsv-seq/rsvpipe --adapter path/to/adapters.fasta -d path/to/fastq_folder
```

Note: Ensure that you replace `path/to/` placeholders with the actual paths relevant on your system.


### 6.4 Activating Primer Clipping

Primer sequences from amplicons can sometimes interfere with variant calling, potentially diluting variant signals. To ensure accuracy, these primer sequences have to be clipped. 

For primer clipping, you'll need to provide a BEDPE file via the `--primer` option. This file should contain primer information in the following tab-delimited fields:

    1. field: forward primer chromosome name
    2. field: forward primer start position
    3. field: forward primer end position
    4. field: reverse primer chromosome name
    5. field: reverse primer start position
    6. field: reverse primer end position
    7. field: auxiliary information (e.g., amplicon name)

**Example**:

```
NC_045512.2     333     350     NC_045512.2     450     470     Amplicon1
NC_045512.2     876     899     NC_045512.2     995     1005    Amplicon2
```

#### Running Primer Clipping

To clip primers based on positions specified in a BEDPE file, use the following command:

```bash
# Clipping primer sequences as specified in primers.bedpe
path/to/next-rsv-seq/rsvpipe --primer path/to/primers.bedpe -d path/to/fastq_folder
```

Note: Replace `path/to/` placeholders with the actual paths specific to your setup.

### 6.5 Refining Read Trimming and Filtering Parameters

Reads are trimmed based on base call qualities to increase mapping and thus consensus accuracy.

#### Setting Minimum Read Length

The default setting for the minimum read length is 50 bases.

To adjust this parameter, the following option can be used in the command:

```bash
# Adpating minimal read length to 25 bases
path/to/next-rsv-seq/rsvpipe -l 25 -d path/to/fastq_folder
```

Note: Replace `path/to/` placeholders with the actual paths specific to your setup.

#### Specifying Read Quality Threshold

By default, the processing pipeline is configured to only retain reads that exhibit an average Phred score of 20 or higher. 
This threshold ensures that, on average, the base call accuracy for a read is 99% or above, providing a balance between 
data quality and throughput.

```bash
# Setting the average Phred score for reads to at least 30
path/to/next-rsv-seq/rsvpipe -p 30 -d path/to/fastq_folder
```

Note: Replace `path/to/` placeholders with the actual paths specific to your setup.


### 6.6 Removing PCR Duplicates

To eliminate PCR duplicates from the sequencing data, initiate a deduplication process using the `u` flag, which identifies and filters 
out sequences that are replicates generated by PCR amplification.

```bash
# Activating PCR deduplication
path/to/next-rsv-seq/rsvpipe -u -d path/to/fastq_folder
```

Note: Replace `path/to/` placeholders with the actual paths specific to your setup.


### 6.7 Enabling Taxonomic Read Filtering

Filtering out contamination in a set of sequencing data is a critical step to ensure consensus accuracy.
To exclude reads from non-RSV sources, taxonomic classification using k-mer frequency analysis which aligns with a specified kraken2 database can be invoked through the `--kraken` option. 
A curated database, which includes sequences from RSV A and B and the human genome, is available for download at:

https://zenodo.org/records/8133844

For research focusing on organisms other than RSV, the target taxonomy ID can be specified using the --taxid option, with the default set to 12814 for RSV. 

```bash
# Filtering for reads assigned to RSV utilizing the kraken2 database located in krakenDB
path/to/next-rsv-seq/rsvpipe --krakne krankenDB -d path/to/fastq_folder
```

Note: Replace `path/to/` placeholders with the actual paths specific to your setup.

### 6.8 Adapting Variant Calling

The precision of variant calling can be enhanced by applying constraints to the sites being analyzed, based on defined criteria at their respective genomic locations:
    - Exclude sites below a certain sequencing depth threshold using `--dd` (default setting is a depth of 20).
    - Disregard bases that are supported by a read count lower than the specified limit with `--dr` (default is 10 reads).
    - Omit bases with a base call quality below the set value with `--dq` (default quality score: 20).
    - Do not consider single nucleotide variants (SNVs) that have p-values above a specific threshold for consensus calling with `--dp`; this is relevant when `--cns custom` is in use (default p-value threshold: 0.05).

These parameters offer a customizable framework for variant calling, allowing for the exclusion of low-confidence data points and the inclusion of only the most reliable information in the analysis.

```bash
# Adapting variant callin g to be less strict
path/to/next-rsv-seq/rsvpipe --dd 10 --dr 5 --dq 15 -d path/to/fastq_folder
```

### 6.9 Adapting Consensus Generation

The process of creating a consensus sequence can be customized through a set of command-line options:
Use an experimental custom-made consensus calling algorithm instead of ivar by flagging `--cns custom`.
Utilize --df to establish a frequency threshold for considering the most abundant bases, based on supporting read evidence. This parameter is effective only when ivar is chosen as the consensus algorithm (default frequency threshold: 0).
Apply --dc to exclude bases for consensus calling that do not meet a specified frequency ratio, which is relevant when the custom consensus algorithm is used (default relative frequency cutoff: 0.1).

### 6.10 Adapting Reporting and Alerts  

The reporting of annotated variants, their predicted effects, and their impact can be customized using two options:  

- `--re` sets the minimum frequency threshold for variants to be included in the Mutation of Interest (MOI) section of the `summary.tsv` report (default: 0.5).  
- `--al` defines the frequency threshold for variants to be considered in the `{sample}_mutation_effects.tsv` file and the alert system (default: 0.05).  

Additionally, the output directory can be specified using the `--out` option (default: `results`). If the directory does not exist, it will be created. Existing directories and files will be overwritten or reused as needed.  

```bash
# Include mutations with at least 60% frequency in the MOI section of `summary.tsv`
path/to/next-rsv-seq/rsvpipe --re 0.6

# Include mutations with at least 10% frequency in the alert system and `{sample}_mutation_effects.tsv`
path/to/next-rsv-seq/rsvpipe --al 0.1

# Write all output to the `my_results` directory
path/to/next-rsv-seq/rsvpipe --out my_results
```

### 6.11 General Options for Tool Configuration

When configuring the tool for running analyses, the following general options are available to tailor the execution environment and output management:
- `--show-refs`: Display a list of all built-in references and then exit the program.
- `--threads INT`: Specify the number of processing threads to be used for the analysis (the default is set to 20).
- `--keep-temp`: Preserve all temporary files generated during the analysis instead of deleting them after completion.
- `--rerun-incomplete`: If an analysis was previously interrupted or incomplete, this option allows it to be restarted from where it left off.
- `-v`, `--version`: Output the version number of the program and exit.

Note: Ensure all paths and options are correctly set according to your specific requirements and system configuration before initiating the analysis.
