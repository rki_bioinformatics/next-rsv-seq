#!/usr/bin/env python

from Bio import SeqIO
import parasail

def calculate_identity(seq1, seq2):
    """
    Perform global alignment using Parasail's Needleman-Wunsch implementation
    with no gap penalties and a simple substitution matrix.
    """
    # Create a simple substitution matrix for nucleotides (or adjust as needed).
    # Here we use a match score of 1 and a mismatch penalty of -1.
    matrix = parasail.matrix_create("ACGT", 1, -1)
    
    # Perform the global alignment with gap open and extension penalties set to 0.
    result = parasail.nw_trace(str(seq1), str(seq2), 0, 0, matrix)
    
    # Get the aligned sequences from the traceback.
    aligned_seq1 = result.traceback.query
    aligned_seq2 = result.traceback.ref

    # Calculate percent identity.
    matches = sum(1 for a, b in zip(aligned_seq1, aligned_seq2) if a == b)
    percent_identity = (matches / len(aligned_seq1)) * 100
    return percent_identity


# Get input and output files from the snakemake object.
query_file = snakemake.input.query
candidate_files = snakemake.input.candidates  # This is a list of candidate files.
output_file = snakemake.output[0]

# Load the query sequence (assumes one sequence per FASTA file).
query_record = next(SeqIO.parse(query_file, "fasta"))
query_seq = query_record.seq


best_identity = -1.0
best_candidate = None

# Loop through each candidate FASTA file.
for candidate_file in candidate_files:
    candidate_record = next(SeqIO.parse(candidate_file, "fasta"))
    candidate_seq = candidate_record.seq
    candidate_accession = candidate_record.id
    identity = calculate_identity(query_seq, candidate_seq)
    if identity > best_identity:
        best_identity = identity
        best_candidate = candidate_accession.split(".")[0]

# Write the result: the candidate accession with the highest identity.
with open(output_file, "w") as out:
    out.write(f"{best_candidate}\n")
