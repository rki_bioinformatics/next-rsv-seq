#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author: Stephan Fuchs (Robert Koch Institute, FG13, fuchss@rki.de)

VERSION = "0.0.9"
import os
import argparse
import re
from Bio import SeqIO
import sys
import random
import time
import gzip
import subprocess
import tempfile
import pandas as pd
import collections

def parse_args(CMD=None):
	parser = argparse.ArgumentParser(prog="sim_amplicon.py", description="creates an consensus based on ivar variant tsv", )
	parser.add_argument('--tsv', metavar='VARIANT_FILE', help="ivar variant output", type=str)
	parser.add_argument('--fasta', metavar='REFERENCE_FILE', help="reference genome sequence in FASTA format", type=str)
	parser.add_argument('--pileup', metavar='PILE_UP', help="pileup file must be gz compressed)", type=str)
	parser.add_argument('--id', metavar='STR', help="consensus id used as fasta header", type=str)
	parser.add_argument('-p', '--maxpval', metavar='FLOAT', help="max p-value of fisher exact's test to consider a base (default: 0.05)", type=float, default=0.05)
	parser.add_argument('-f', '--minfreq', metavar='FLOAT', help="minimal frequency that a base is considered (default: 0.1)", type=float, default=0.1)
	parser.add_argument('-q', '--minqual', metavar='INT', help="minimal quality that a base is considered (default: 15)", type=int, default=15)
	parser.add_argument('-c', '--mincount', metavar='INT', help="minimal absolut number of supporting reads that a base is considered (default: 10)", type=int, default=10)
	parser.add_argument('-d', '--mindepth', metavar='INT', help="minimal depth (default: 20)", type=int, default=20)
	parser.add_argument('-o', '--outfile', metavar='STR', help="output fasta file stroing generated consensus (will be overwritten if existing)", type=str)
	parser.add_argument('-l', '--log', metavar='STR', help="log file (will be overwritten if existing, default: no logging)", type=str, default=None)
	return parser.parse_args(CMD)

def read_fasta(fname):
	record = SeqIO.read(fname, "fasta")
	return record.id, str(record.seq)

def process_tsv(fname):
	return pd.read_csv(fname)

def decode_phred(phred_score):
	return ord(phred_score)-33

def check_variant(df, seqid, alt, pos, max_pval):
	return ((df['REGION'] == seqid) & (df['POS'] == pos) & (df['ALT'] == alt) & (df['PVAL'] <= max_pval)).any()

def intdict():
	return collections.defaultdict(int)

def pilupdict():
	return collections.defaultdict(intdict)

def create_cns(min_basequality, minfreq, mincount, mincov, maxpval, reffile, variantfile, pileupfile, logfile=None):
	refid, refseq = read_fasta(reffile)
	reflen = len(refseq)
	vdf = pd.read_csv(variantfile, sep="\t")
	pileup = collections.defaultdict(pilupdict)
	gapped_depth = collections.defaultdict(int)
	ignore = re.compile(r'[><$]|\^.')
	pattern = re.compile(r'[+-][0-9]+|.')
	REF = set(",.")
	ALT = set("ACGTNacgtn")
	NUM = set("1234567890")
	REFID = refid
	IUPAC = {'*': '*', 'A': 'A', 'C': 'C', 'G': 'G', 'T': 'T', 'AG': 'R', 'CT': 'Y', 'CG': 'S', 'AT': 'W', 'GT': 'K', 'AC': 'M', 'CGT': 'B', 'AGT': 'D', 'ACT': 'H', 'ACG': 'V', 'ACGT': 'N'}

	cns = ['.' for x in refseq]

	if logfile:
		loghandle = open(logfile, "w")

	with gzip.open(pileupfile, 'rt') as handle:
		lno = 0
		for line in handle:
			lno += 1
			line = line.strip()
			if len(line) == 0:
				continue

			seqid, pos, ref, ungapped_depth, pile, quals = line.split("\t")

			if seqid != REFID:
				continue
			if pile == "*" or not pile:
				continue

			pos = int(pos)
			ungapped_depth = int(ungapped_depth)
			pile = ignore.sub("", pile)
			pile_size = len(pile)
			quals = list(quals[::-1])

			# extract relevant individual calls from pile
			matchlist = pattern.findall(pile)
			indel_char = set("+-")
			calls = []
			l = len(matchlist) - 1
			i = -1

			#register calls
			while i < l:
				i += 1
				# registering ref call
				if matchlist[i] in REF:
					if decode_phred(quals.pop()) >= min_basequality:
						calls.append(ref)

				# registering alt call
				elif matchlist[i] in ALT:
					if decode_phred(quals.pop()) >= min_basequality and check_variant(vdf, seqid, matchlist[i], pos, maxpval):
						calls.append(matchlist[i].upper())

				# registering insert
				elif matchlist[i][0] == "+":
					e = int(matchlist[i][1:])
					insert = "".join(matchlist[i+1:i+e+1])
					if check_variant(vdf, seqid, "+" + insert, pos, maxpval):
						calls.append(insert.upper())
					i += e

				# registering deletion
				elif matchlist[i] == "*":
					if decode_phred(quals.pop()) >= min_basequality:
						calls.append("*")
				elif matchlist[i][0] == "-":
					i += int(matchlist[i][1:])

				# raising error if unknown char found
				else:
					sys.exit("error: unkown base call in pileup file (" + matchlist[i] + ")")

			if quals:
				sys.exit("pileup error: more quality values than calls in line " + str(lno) + " of pileup file.")

			gapped_depth = len(calls)
			if gapped_depth < mincov:
				cns[pos-1] = "N"
				if logfile:
					loghandle.write(str(pos) + "\t" + cns[pos-1] + "\t" + "low coverage (" + str(gapped_depth) + "<" + str(mincov) + ")\n")
			else:
				support_sum = { x: calls.count(x) for x in set(calls) }
				survived_calls = { x: y / gapped_depth for x, y in support_sum.items() if y >= mincount and y / gapped_depth >= minfreq }
				if (("N" in survived_calls or "*" in survived_calls) and len(survived_calls) > 1) or len(survived_calls) == 0:
					cns[pos-1] = "N" * max([len(x) for x in survived_calls])
					if logfile:
						loghandle.write(str(pos) + "\t" + cns[pos-1] + "\t" + str(survived_calls) + "\n")
				else:
					cns[pos-1] = ""
					for x in zip(*survived_calls.keys()):
						if len(set(x)) != len(survived_calls):
							cns[pos-1] += "N"
						else:
							x = "".join(sorted(x))
							cns[pos-1] += IUPAC[x]
						if logfile and len(survived_calls.keys()) > 1:
							loghandle.write(str(pos) + "\t" + cns[pos-1] + "\t" + x + " -> " + IUPAC[x] + "\n")

	if logfile:
		loghandle.close()

	return "".join(cns).strip(".").replace(".", "N").replace("*", "")

def get_cmd_from_snake(snakemake):
    cmd = ["--tsv", snakemake.input['variant'],
	       "--pileup", snakemake.input['pileup'],
	       "--fasta", snakemake.input['reference'],
	       "--outfile", snakemake.output[0],
	       "--log", snakemake.log,
	       "--id", snakemake.wildcards['sample'],
		   "--minfreq", snakemake.params['cns_minfreq'],
		   "--mindepth", snakemake.params['cns_mincov'],
		   "--minqual", snakemake.params['base_minqual'],
		   "--maxpval", snakemake.params['cns_maxpval'],
		   "--mincount", snakemake.params['cns_mincount']]
    cmd = [str(arg) for arg in cmd]
    return cmd


def main(CMD = None):
	args = parse_args(CMD)
	with open(args.outfile, "w") as handle:
		handle.write(">" + args.id + "\n" + create_cns(args.minqual, args.minfreq, args.mincount, args.mindepth, args.maxpval, args.fasta, args.tsv, args.pileup, args.log))

if __name__ == "__main__":
    CMD = None
    if "snakemake" in globals():
        CMD = get_cmd_from_snake(snakemake)
    main(CMD=CMD)
