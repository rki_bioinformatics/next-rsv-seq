#!/usr/bin/env python3
import sys
import pandas as pd
import pysam
import csv
from pathlib import Path
import re


def parse_snpeff_vcf(vcf_file, min_alt_freq, min_total_dp):
    """Parses a SnpEff-annotated VCF file into a pandas DataFrame for filtering."""
    data = []
    with open(vcf_file, 'r') as file:
        for line in file:
            if line.startswith("#"):  # Skip header lines
                continue
            columns = line.strip().split("\t")
            chrom, pos, id_, ref, alt, qual, filt, info = columns[:8]            
            
            # Filter by PASSED  
            if filt == 'FALSE':
                continue
            
            # Filter by ALT-FREQ 
            freq_match = re.search(r'ALT_FREQ=([^;]+)', info)
            try:
                alt_freq = float(freq_match.group(1)) 
            except ValueError:
                raise ValueError(f"Incorrect ALT_FREQ value in '{vcf_file}': {freq_match.group(1)}")     
            if alt_freq < min_alt_freq:
                continue
 
             # Filter by TOTAL_DP 
            dp_match = re.search(r'TOTAL_DP=([^;]+)', info)
            try:
                total_dp = int(dp_match.group(1)) 
            except ValueError:
                raise ValueError(f"Incorrect TOTAL_DP value in '{vcf_file}': {total_dp.group(1)}")     
            if total_dp < min_total_dp:
                continue          
            
            # Extract ANN= field from INFO column.
            ann_match = re.search(r'ANN=([^;]+)', info)
            if not ann_match:
                continue  # Skip if no ANN field
            ann_field = ann_match.group(1)
            freq = freq_match.group(1)
            dp = dp_match.group(1)
            annotations = ann_field.split(",")  # Multiple effects possible
            for ann in annotations:
                fields = ann.split("|")
                if len(fields) < 15:  # Ensure we have enough fields
                    continue
                allele, annotation, impact, gene_name, gene_id, feature_type, feature_id, biotype, rank_total, \
                hgvs_c, hgvs_p, cdna_pos, cds_pos, aa_pos, distance = fields[:15]
                data.append({
                    "CHROM": chrom,
                    "POS": int(pos),
                    "REF": ref,
                    "ALT": allele,
                    "ALT_FREQ": float(alt_freq),
                    "TOTAL_DP": int(total_dp),
                    "PASS": filt,
                    "Gene": gene_name,
                    "Effect": annotation,
                    "Impact": impact,
                    "HGVS_c": hgvs_c,   # cDNA change
                    "HGVS_p": hgvs_p,   # Protein change in HGVS format
                    "AA_Pos": aa_pos,   # Amino acid position
                    "Region": feature_type
                })                
    df = pd.DataFrame(data)
    return df

def lift_coordinate(chain_path, chrom, pos):
    """
    Given a UCSC chain file, find a chain whose target chromosome (tName)
    matches `chrom` and that covers the coordinate `pos`. If found, return the corresponding
    query coordinate (which may be identical to pos); if no chain covers the position, return None.
    
    This simplified parser assumes:
      - The chain file is in standard UCSC chain format.
      - A chain header starts with "chain" and subsequent block lines provide block sizes and gap information.
    """
    with open(chain_path, 'r') as f:
        for line in f:
            line = line.strip()
            if not line:
                continue
            if line.startswith("chain"):
                # Header format:
                # chain score tName tSize tStrand tStart tEnd qName qSize qStrand qStart qEnd id
                parts = line.split()
                tName = parts[2]
                tStart = int(parts[5])
                tEnd = int(parts[6])
                qStrand = parts[9]
                qStart = int(parts[10])
                qEnd = int(parts[11])
                if tName == chrom and tStart <= pos < tEnd:
                    current_t = tStart
                    current_q = qStart if qStrand == '+' else qEnd
                    for line in f:
                        line = line.strip()
                        if not line:
                            continue
                        if line.startswith("chain"):
                            break
                        parts = line.split()
                        block_size = int(parts[0])
                        if current_t <= pos < current_t + block_size:
                            offset = pos - current_t
                            return current_q + offset if qStrand == '+' else current_q - offset
                        if len(parts) == 3:
                            dt = int(parts[1])
                            dq = int(parts[2])
                            current_t += block_size + dt
                            current_q = current_q + block_size + dq if qStrand == '+' else current_q - (block_size + dq)
                        else:
                            current_t += block_size
                    # Fallback: if no block covers the position, consider returning None instead
                    return current_q  
        return None  # No chain found
  
def main():
    # Snakemake inputs and parameters.
    input_tsv = snakemake.input.ivar_tsv
    gff_path = snakemake.params.gff
    chain_file = snakemake.input.chain
    gene_name = snakemake.params.gene_name
    snpeff_ref = snakemake.input.snpeff_ref
    snpeff_vcf = snakemake.input.snpeff_vcf
    output_csv = snakemake.output.csv
    minor_af_threshold = snakemake.params.minor_af_threshold
    main_af_threshold = snakemake.params.main_af_threshold
    sample = snakemake.wildcards.sample
    treshold = float(snakemake.params.alert_treshold)
    min_coverage = int(snakemake.params.min_cov)    

    # Read SNPeff reference accession.
    snpeff_ref_acc = Path(snpeff_ref).read_text().strip()

    # Read the ivar TSV file.
    try:
        df = pd.read_csv(input_tsv, sep='\t')
    except FileNotFoundError:
        sys.exit(f"Error: TSV file '{input_tsv}' not found.")
    except pd.errors.EmptyDataError:
        df = pd.DataFrame(columns=["CHROM", "POS", "REF", "ALT", "ALT_FREQ", "TOTAL_DP", "PASS", "Gene", "Effect", "Impact", "HGVS_c", "HGVS_p", "AA_Pos", "Region"])

    df['SAMPLE'] = sample
    df = df[(df["ALT_FREQ"] >= treshold) & (df["TOTAL_DP"] >= min_coverage)]
    
    if 'REGION' in df.columns:
        df.rename(columns={'REGION': 'MAPPING_REFERENCE'}, inplace=True)
    elif 'REFERENCE' in df.columns:
        df.rename(columns={'REFERENCE': 'MAPPING_REFERENCE'}, inplace=True)

    df.rename(columns={'POS': 'MAPPING_POS'}, inplace=True)

    # Compute lifted positions using the UCSC chain file.
    if 'MAPPING_REFERENCE' in df.columns:
        df['LIFTED_POS'] = df.apply(lambda row: lift_coordinate(chain_file, row['MAPPING_REFERENCE'], row['MAPPING_POS']), axis=1)
    else:
        df['LIFTED_POS'] = None

    # Parse SnpEff VCF annotations.
    df_snpeff = parse_snpeff_vcf(snpeff_vcf, treshold, min_coverage)

    # Process all variants (no gene coordinate filtering).
    df['ALT_FREQ'] = pd.to_numeric(df['ALT_FREQ'], errors='coerce').fillna(0)
    df['Variant_Type'] = df['ALT_FREQ'].apply(
        lambda af: 'Minor' if af < minor_af_threshold else ('Major' if af >= main_af_threshold else 'Neutral')
    )

    # Merge with SnpEff annotations using the lifted position.
    df_merged = df.merge(df_snpeff, left_on=['LIFTED_POS', 'ALT'], right_on=['POS', 'ALT'], how='left', suffixes=("", "_snpeff"))

    # Override the 'POS' column for final output:
    # If a mapping exists (i.e. LIFTED_POS is not None), output that numeric value; otherwise, output "not lifted".
    df_merged['POS'] = df_merged['LIFTED_POS'].apply(lambda x: x if x is not None else "not lifted")

    # Add SNPeff reference in the desired column.
    df_merged['REFERENCE'] = snpeff_ref_acc
    
    # Set desired column order.
    desired_order = [
        'SAMPLE', 'MAPPING_REFERENCE', 'MAPPING_POS', 'REFERENCE', 'POS',
        'REF', 'ALT', 'REF_DP', 'ALT_DP', 'TOTAL_DP', 'ALT_FREQ', 'PVAL', 'PASS',
        'Variant_Type', 'Effect', 'Impact', 'Gene', "HGVS_c", "HGVS_p", "AA_Pos", "Region"
    ]
    desired_order = [col for col in desired_order if col in df_merged.columns]
    df_merged = df_merged[desired_order]

    # Save combined CSV (tab-delimited) with every field encapsulated by double quotes.
    df_merged.to_csv(output_csv, index=False, sep='\t', quoting=csv.QUOTE_ALL, quotechar='"')

if __name__ == "__main__":
    main()
