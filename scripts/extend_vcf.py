#!/usr/bin/env python3
import csv
import argparse
import gzip

def open_mpileup(file_path):
    """Open mpileup file, handling gzipped files if necessary."""
    if file_path.endswith(".gz"):
        return gzip.open(file_path, "rt")
    else:
        return open(file_path, "r")

def main():
    # If run via Snakemake, the 'snakemake' variable will be available.
    if "snakemake" in globals():
        args = argparse.Namespace(
            ivar=snakemake.input.tsv,
            mpileup=snakemake.input.pileup,
            output=snakemake.output.tsv,
            mincov=snakemake.params.mincov
        )
    else:
        parser = argparse.ArgumentParser(
            description="Merge iVar variant TSV with mpileup (gz compressed supported) to add missing positions and include region information"
        )
        parser.add_argument("--ivar", required=True,
                            help="Path to iVar variant TSV file")
        parser.add_argument("--mpileup", required=True,
                            help="Path to samtools mpileup file (can be gz compressed)")
        parser.add_argument("--output", required=True,
                            help="Path to output merged TSV file")
        parser.add_argument("--mincov", type=int, required=True,
                            help="Minimum coverage threshold; positions with lower coverage will be skipped")
        args = parser.parse_args()

    ivar_variants = {}
    region_value = None  # To store the region from the iVar file

    # Read the iVar variant TSV file.
    with open(args.ivar, "r") as ivar_file:
        reader = csv.DictReader(ivar_file, delimiter="\t")
        header_mapping = {field.lower(): field for field in reader.fieldnames}
        for row in reader:
            # Set region_value from the first row (assumes the same region for all rows)
            if region_value is None:
                try:
                    region_value = row[header_mapping["region"]]
                except KeyError:
                    print("The 'REGION' column is missing in the iVar file header.")
                    return

            try:
                pos = int(row[header_mapping["pos"]])
            except (ValueError, KeyError):
                print("Skipping row due to missing or invalid 'POS' value:", row)
                continue
            ivar_variants[pos] = row

    # Open the output file for writing.
    with open(args.output, "w", newline="") as out_file:
        writer = csv.DictWriter(out_file, fieldnames=reader.fieldnames, delimiter="\t")
        writer.writeheader()

        # Process the mpileup file.
        with open_mpileup(args.mpileup) as mp_file:
            for line in mp_file:
                parts = line.strip().split("\t")
                if len(parts) < 4:
                    continue  # skip malformed lines
                try:
                    pos = int(parts[1])
                except ValueError:
                    continue
                ref_base = parts[2]
                depth = parts[3]

                if int(depth) < args.mincov:
                    continue

                if pos in ivar_variants:
                    writer.writerow(ivar_variants[pos])
                else:
                    # Create a default row for positions with no variant call.
                    default_row = {}
                    for col in reader.fieldnames:
                        col_upper = col.upper()
                        if col_upper == "REGION":
                            default_row[col] = region_value
                        elif col_upper == "POS":
                            default_row[col] = pos
                        elif col_upper in ("REF", "ALT"):
                            default_row[col] = ref_base
                        elif col_upper == "REF_DP":
                            default_row[col] = 0
                        elif col_upper in ("TOTAL_DP", "ALT_DP"):
                            default_row[col] = depth
                        elif col_upper == "ALT_FREQ":
                            default_row[col] = 1
                        else:
                            default_row[col] = "NA"
                    writer.writerow(default_row)

if __name__ == "__main__":
    main()
