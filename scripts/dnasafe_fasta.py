#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author: Stephan Fuchs (Robert Koch Institute, FG13, fuchss@rki.de)

VERSION = "0.0.10"
import os
import re
import sys
import argparse
import textwrap
from Bio import SeqIO

def parse_args(CMD=None):
    parser = argparse.ArgumentParser(prog="dna_safe.py", description="writes a dna-safe fasta file")
    parser.add_argument('-f', '--fasta', metavar='FASTA_FILE', help="fasta file to check", type=str, required=True)
    parser.add_argument('-o', '--out', metavar='FASTA_FILE', help="dna-safe fasta file to write (existing file will be overwritten!). If not specified, the input fasta file will be modified in-place.", type=str, default=None)
    parser.add_argument('-l', '--len', metavar='INT', help="sequence line length (set 0 to avoid new lines, default=60)", type=int, default=60)
    parser.add_argument('-a', '--acc', metavar='STR', help="use this accession as fasta header", type=str, default=None)
    parser.add_argument('--version', action='version', version='%(prog)s ' + VERSION)
    return parser.parse_args(CMD)

def get_cmd_from_snake(snakemake):
    cmd = ["-o", snakemake.output[0], "--fasta", snakemake.input[0]]
    cmd = [str(arg) for arg in cmd]
    return cmd

def main():
    valid_letters = "ATGCURYSWKMBDHVN.-"

    # args
    CMD = None
    if "snakemake" in globals():
        CMD = get_cmd_from_snake(snakemake)
    args = parse_args(CMD=CMD)

    # check file
    if not os.path.isfile(args.fasta):
        sys.exit("error: the input fasta file does not exist")

    # read and process sequences
    sequences = []
    for record in SeqIO.parse(args.fasta, "fasta"):
        if args.acc:
            header = args.acc
        else:
            header = str(record.id)
        seq = str(record.seq).upper()
        if not all(i in valid_letters for i in seq):
            sys.exit("error: sequence '" + header + "' in " + args.fasta + " contains non-IUPAC characters.")
        seq = seq.replace(".", "").replace("-", "").replace("U", "T")
        if args.len > 0:
            seq = textwrap.fill(seq, width=args.len)
        sequences.append((header, seq))

    # write sequences to output
    output_file = args.out if args.out else args.fasta
    with open(output_file, "w") as handle:
        for header, seq in sequences:
            handle.write(">" + header + "\n" + seq + "\n")

if __name__ == "__main__":
    main()
