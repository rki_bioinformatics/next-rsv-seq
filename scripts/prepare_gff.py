#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import re


def generate_unique_id(id_set, base_id='gene'):
    """
    Generate a unique ID by appending an incrementing number to base_id,
    ensuring it's not in the given id_set.
    """
    count = 1
    new_id = f'{base_id}_{count}'
    while new_id in id_set:
        count += 1
        new_id = f'{base_id}_{count}'
    id_set.add(new_id)
    return new_id

def get_attr_value(attrs, name):
    if name not in attrs:
        return name
    else:
        return attrs[name][0]
  
def parse_attributes(attr_string):
    """
    Parse a GFF3 attributes string in the format:
      key1=value1;key2=value2;...
    Returns a dictionary mapping keys to values.
    Extraneous surrounding quotes are removed.
    """
    attr_string = attr_string.strip().strip('"')
    attr_dict = {}
    for part in attr_string.split(';'):
        part = part.strip()
        if not part:
            continue
        if '=' in part:
            key, value = part.split('=', 1)
            attr_dict[key.lower().strip()] = (value.strip().strip('"'), part)
    return attr_dict

def format_attributes(attr_dict):
    """
    Format a dictionary into a GFF3 attributes string in the style:
      key1=value1;key2=value2;
    """
    if not attr_dict:
        return ""
    return ";".join(f"{value[1]}" for value in attr_dict.values()) + ";"

def process(input_file, output_file):
    rows = [] 
    with open(input_file, 'r') as infile:
         for line in infile:
            rows.append([x.strip() for x in line.split("\t")])

    output_lines = []
    # Maintain sets for known IDs (genes, transcripts, other).
    existing_ids = {
        "genes": set(),
        "transcripts": set(),
        "other": set()
    }

    # Track "current" gene and transcript as we move through the file
    current_gene_id = None
    current_transcript_id = None

    for row in rows:
        # Pass through comment/header lines unchanged
        print(input_file)
        if row[0].startswith('##sequence-region'):
            if len(row[0]) == 1: 
                row = re.split(r'\s+', row[0].strip()) 
            output_lines.append("\t".join(row))
            continue
        if len(row) < 9 or row[0].startswith('#'):
            output_lines.append("\t".join(row))
            continue

        feature_type = row[2].lower()
        attrs = parse_attributes(row[8])

        if feature_type == 'gene':
            # --- GENE FEATURE ---
            # Ensure a ID
            if not get_attr_value(attrs, "id"):
                new_gene_id = generate_unique_id(existing_ids["genes"], feature_type)
                attrs['id'] = (new_gene_id, f"ID={new_gene_id}")
            current_gene_id = attrs['id'][0]
            existing_ids["genes"].add(current_gene_id)
            
            # Ensure a name
            if 'name' not in attrs:
                attrs['name'] = (current_gene_id, f"Name={current_gene_id}")

            # Update row with possibly modified attributes
            row[8] = format_attributes(attrs)

            # Reset current transcript
            current_transcript_id = None
            
            # Prepare Output
            output_lines.append("\t".join(row))

        elif feature_type == 'transcript':
            # --- TRANSCRIPT FEATURE ---
            # Ensure a ID
            if not get_attr_value(attrs, "id"):
                new_transcript_id = generate_unique_id(existing_ids["transcripts"], feature_type)
                attrs['id'] = (new_transcript_id, f"ID={new_transcript_id}")
                
            elif get_attr_value(attrs, "id") in existing_ids["genes"]:
                new_transcript_id = get_attr_value(attrs, "id") + "-transcript"
                attrs['id'] = (new_transcript_id, f"ID={new_transcript_id}")

            if not get_attr_value(attrs, "parent"):
                attrs['parent'] = (current_gene_id, f"Parent={current_gene_id}")

            current_transcript_id = attrs['id'][0]
            existing_ids["transcripts"].add(current_transcript_id)
            row[8] = format_attributes(attrs)
            output_lines.append("\t".join(row))

        elif feature_type in ['exon', 'cds', 'start_codon', 'stop_codon', 'utr']:

            # --- EXON/CDS/UTR FEATURE ---
            # Ensure a ID
            if not get_attr_value(attrs, "id"):
                new_id = generate_unique_id(existing_ids["other"], feature_type)
                attrs['id'] = (new_id, f"ID={new_id}")

            existing_ids["other"].add(attrs['id'][0])

            # Check for parent
            add_parent=False
            provided_parent = get_attr_value(attrs, "parent")

            if provided_parent:
                if provided_parent in existing_ids["transcripts"]:
                    current_transcript_id = provided_parent
                else:
                    current_transcript_id = provided_parent
                    add_parent = True
            else:
                current_transcript_id = generate_unique_id(existing_ids["transcripts"], 'transcript')
                add_parent = True
                
            if current_transcript_id in existing_ids["genes"]:
                current_transcript_id = current_transcript_id + "-transcript"
                i = 2
                while current_transcript_id in existing_ids["transcripts"]:
                    current_transcript_id = current_transcript_id + str(i)
                    i += 1
                    
                    
            if add_parent:
                p_attrs = {}
                existing_ids["transcripts"].add(current_transcript_id)
                transcript_row = row.copy()
                transcript_row[2] = 'transcript'

                p_attrs['id'] = (current_transcript_id, f"ID={current_transcript_id}")
                
                if current_gene_id:
                    p_attrs['parent'] = (current_gene_id, f"Parent={current_gene_id}")

                transcript_row[8] = format_attributes(p_attrs)
                output_lines.append("\t".join(transcript_row)) 
            
            existing_ids["transcripts"].add(current_transcript_id)     

            # Override the feature's parent with the transcript ID
            attrs['parent'] = (current_transcript_id, f"Parent={current_transcript_id}")

            row[8] = format_attributes(attrs)
            output_lines.append("\t".join(row))

        #else:
            #
            # --- ANY OTHER FEATURE ---
            # Pass it through unchanged.
            # output_lines.append("\t".join(row))

    # Write out all lines
    with open(output_file, 'w') as outfile:
        outfile.write("\n".join(output_lines) + "\n")

def main():
    parser = argparse.ArgumentParser(
        description="Add missing transcript features and link CDS/exon/UTR features to transcripts, "
                    "optionally generating new transcripts if needed."
    )
    parser.add_argument("input_file", help="Path to the input GFF file")
    parser.add_argument("output_file", help="Path to the output GFF file")
    args = parser.parse_args()
    process(args.input_file, args.output_file)

if __name__ == "__main__":
    main()
