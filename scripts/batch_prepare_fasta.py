#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author: Stephan Fuchs (Robert Koch Institute, FG13, fuchss@rki.de)

import csv
import os
import argparse

def prepare(input_file, output_file): 
    with open(input_file, 'r') as infile:
        with open(output_file, 'w') as outfile:
            for line in infile:
                line = line.strip()
                if line.startswith(">"):
                    line = line.split(".")[0]
                outfile.write(line + "\r\n")
            outfile.write("\r\n")


def main():
    parser = argparse.ArgumentParser(description="Prepare FASTA file.")
    parser.add_argument("input_files", help="The path to the input FASTA file", nargs="+")
    parser.add_argument("-d", help="output directory", required=True)
    parser.add_argument("-f", help="force overwriting", action="store_true")

    args = parser.parse_args()

    for input_file in args.input_files: 
        output_file = os.path.join(os.path.realpath(args.d), os.path.basename(os.path.realpath(input_file)))
        if not args.f and os.path.isfile(output_file):
            exit("error: " + output_file + " already exists.")

        prepare(input_file, output_file)

if __name__ == "__main__":
    main()
