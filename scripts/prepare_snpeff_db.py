#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import shutil
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

# ===============================================================
# PART 1: GFF PROCESSING FUNCTIONS 
# ===============================================================

def generate_unique_id(id_set, base_id='gene'):
    """
    Generate a unique ID by appending an incrementing number to base_id,
    ensuring it's not in the given id_set.
    """
    count = 1
    new_id = f'{base_id}_{count}'
    while new_id in id_set:
        count += 1
        new_id = f'{base_id}_{count}'
    id_set.add(new_id)
    return new_id

def get_attr_value(attrs, name):
    # Returns the attribute value if present, otherwise returns an empty string.
    # (Note: the original script returned the name if missing, but here we return ""
    # so that tests like “if not get_attr_value(attrs, 'id')” work as intended.)
    if name not in attrs:
        return ""
    else:
        return attrs[name][0]
  
def parse_attributes(attr_string):
    """
    Parse a GFF3 attributes string in the format:
      key1=value1;key2=value2;...
    Returns a dictionary mapping keys to a tuple of (value, original_key_value_string).
    Extraneous surrounding quotes are removed.
    """
    attr_string = attr_string.strip().strip('"')
    attr_dict = {}
    for part in attr_string.split(';'):
        part = part.strip()
        if not part:
            continue
        if '=' in part:
            key, value = part.split('=', 1)
            key_clean = key.lower().strip()
            attr_dict[key_clean] = (value.strip().strip('"'), part)
    return attr_dict

def format_attributes(attr_dict):
    """
    Format a dictionary into a GFF3 attributes string in the style:
      key1=value1;key2=value2;
    """
    if not attr_dict:
        return ""
    # Use the original "key=value" strings for output.
    return ";".join(f"{value[1]}" for value in attr_dict.values()) + ";"

def process(input_file, output_file):
    """
    Process the input GFF file to ensure that gene, transcript, and
    CDS/exon/UTR features have proper IDs and parent attributes.
    Writes the modified content to output_file.
    """
    with open(input_file, 'r') as infile:
        rows = [line.rstrip("\n").split("\t") for line in infile]

    output_lines = []
    # Maintain sets for known IDs (genes, transcripts, other).
    existing_ids = {
        "genes": set(),
        "transcripts": set(),
        "other": set()
    }

    # Track the "current" gene and transcript as we move through the file.
    current_gene_id = None
    current_transcript_id = None

    for row in rows:
        # Pass through comment/header lines unchanged.
        if row[0].startswith('##sequence-region'):
            row[1] = row[1].split(".")[0]
            output_lines.append("\t".join(row))
            continue
        if len(row) < 9 or row[0].startswith('#'):
            output_lines.append("\t".join(row))
            continue

        feature_type = row[2].lower()
        attrs = parse_attributes(row[8])

        if feature_type == 'gene':
            # --- GENE FEATURE ---
            # Ensure an ID.
            if not get_attr_value(attrs, "id"):
                new_gene_id = generate_unique_id(existing_ids["genes"], feature_type)
                attrs['id'] = (new_gene_id, f"ID={new_gene_id}")
            else:
                current_gene_id = get_attr_value(attrs, "id")
                existing_ids["genes"].add(current_gene_id)
            current_gene_id = attrs['id'][0]
            existing_ids["genes"].add(current_gene_id)
            
            # Ensure a name.
            if 'name' not in attrs:
                attrs['name'] = (current_gene_id, f"Name={current_gene_id}")

            # Update row with possibly modified attributes.
            row[8] = format_attributes(attrs)

            # Reset current transcript.
            current_transcript_id = None
            
            # Append processed line.
            output_lines.append("\t".join(row))

        elif feature_type == 'transcript':
            # --- TRANSCRIPT FEATURE ---
            # Ensure an ID.
            if not get_attr_value(attrs, "id"):
                new_transcript_id = generate_unique_id(existing_ids["transcripts"], feature_type)
                attrs['id'] = (new_transcript_id, f"ID={new_transcript_id}")
            elif get_attr_value(attrs, "id") in existing_ids["genes"]:
                new_transcript_id = get_attr_value(attrs, "id") + "-transcript"
                attrs['id'] = (new_transcript_id, f"ID={new_transcript_id}")

            if not get_attr_value(attrs, "parent"):
                attrs['parent'] = (current_gene_id, f"Parent={current_gene_id}")

            current_transcript_id = attrs['id'][0]
            existing_ids["transcripts"].add(current_transcript_id)
            row[8] = format_attributes(attrs)
            output_lines.append("\t".join(row))

        elif feature_type in ['exon', 'cds', 'start_codon', 'stop_codon', 'utr']:
            # --- EXON/CDS/UTR FEATURE ---
            # Ensure an ID.
            if not get_attr_value(attrs, "id"):
                new_id = generate_unique_id(existing_ids["other"], feature_type)
                attrs['id'] = (new_id, f"ID={new_id}")

            existing_ids["other"].add(attrs['id'][0])

            # Check for parent.
            add_parent = False
            provided_parent = get_attr_value(attrs, "parent")

            if provided_parent:
                if provided_parent in existing_ids["transcripts"]:
                    current_transcript_id = provided_parent
                else:
                    current_transcript_id = provided_parent
                    add_parent = True
            else:
                current_transcript_id = generate_unique_id(existing_ids["transcripts"], 'transcript')
                add_parent = True
                
            if current_transcript_id in existing_ids["genes"]:
                current_transcript_id = current_transcript_id + "-transcript"
                i = 2
                while current_transcript_id in existing_ids["transcripts"]:
                    current_transcript_id = current_transcript_id + str(i)
                    i += 1
                    
            if add_parent:
                p_attrs = {}
                existing_ids["transcripts"].add(current_transcript_id)
                transcript_row = row.copy()
                transcript_row[2] = 'transcript'
                p_attrs['id'] = (current_transcript_id, f"ID={current_transcript_id}")
                
                if current_gene_id:
                    p_attrs['parent'] = (current_gene_id, f"Parent={current_gene_id}")

                transcript_row[8] = format_attributes(p_attrs)
                output_lines.append("\t".join(transcript_row)) 
            
            existing_ids["transcripts"].add(current_transcript_id)     

            # Override the feature's parent with the transcript ID.
            attrs['parent'] = (current_transcript_id, f"Parent={current_transcript_id}")

            row[8] = format_attributes(attrs)
            output_lines.append("\t".join(row))

        else:
            # For any other feature, pass it through unchanged.
            output_lines.append("\t".join(row))

    # Write out all lines.
    with open(output_file, 'w') as outfile:
        outfile.write("\n".join(output_lines) + "\n")

# ===============================================================
# PART 2: CDS/PROTEIN EXTRACTION FUNCTION
# ===============================================================

def extract_cds_from_gff(gff_file, genome_fasta, cds_output, protein_output, translation_table):
    """
    Reads a GFF file, extracts CDS features, and writes the CDS sequences
    and their translated protein sequences to separate FASTA files.
    """
    # Load genome sequences into a dictionary.
    genome = SeqIO.to_dict(SeqIO.parse(genome_fasta, "fasta"))

    cds_records = []
    protein_records = []

    with open(gff_file, "r") as gff:
        for line in gff:
            if line.startswith("#"):
                continue  # Skip comment lines.
            fields = line.strip().split("\t")
            if len(fields) < 9:
                continue  # Ensure valid GFF format.

            seq_id, source, feature_type, start, end, score, strand, phase, attributes = fields

            if feature_type != "CDS":
                continue  # Process only CDS features.

            # Extract coordinates (GFF is 1-based).
            start = int(start) - 1
            end = int(end)

            # Get sequence from genome.
            if seq_id in genome:
                extracted_seq = genome[seq_id].seq[start:end]

                # Reverse complement if on the negative strand.
                if strand == "-":
                    extracted_seq = extracted_seq.reverse_complement()

                # Extract ID from attributes (default to 'unknown' if missing).
                attributes_dict = {key.strip(): value.strip() 
                                   for key, value in [attr.split("=") for attr in attributes.split(";") if "=" in attr]}
                cds_id = attributes_dict.get("Parent", attributes_dict.get("ID", "unknown"))

                # Store CDS sequence.
                cds_records.append(SeqRecord(extracted_seq, id=cds_id, description=""))

                # Translate sequence using specified translation table.
                protein_seq = extracted_seq.translate(table=translation_table, to_stop=True)
                protein_records.append(SeqRecord(protein_seq, id=cds_id, description=""))

    # Write CDS sequences to file.
    SeqIO.write(cds_records, cds_output, "fasta")
    # Write protein sequences to file.
    SeqIO.write(protein_records, protein_output, "fasta")

    print(f"CDS sequences saved to {cds_output}")
    print(f"Protein sequences saved to {protein_output}")
    print(f"Translation table used: {translation_table}")

# ===============================================================
# PART 3: MAIN 
# ===============================================================

if __name__ == "__main__":
    # Retrieve parameters from Snakemake.
    # (Ensure that your Snakemake workflow provides these parameters.)
    accs = snakemake.params.accs            # List of accession names.
    translation_table = int(snakemake.params.ttable)
    source_dir = snakemake.params.ref_dir    # Directory with source FASTA and GFF files.
    target_dir = snakemake.params.out_dir     # Output directory.

    for acc in accs:
        # Define file paths.
        source_fasta = os.path.join(source_dir, f"{acc}.fasta")
        source_gff = os.path.join(source_dir, f"{acc}.gff3")
        source_alerts = os.path.join(source_dir, f"{acc}.alerts")
        
        target_acc_dir = os.path.join(target_dir, acc)
        os.makedirs(target_acc_dir, exist_ok=True)
        
        target_fasta = os.path.join(target_acc_dir, "sequences.fasta")
        target_alerts = os.path.join(target_acc_dir, "..", f"{acc}.alerts")
        target_gff = os.path.join(target_acc_dir, "genes.gff")
        target_cds = os.path.join(target_acc_dir, "cds.fa")
        target_protein = os.path.join(target_acc_dir, "protein.fa")
        target_conf = os.path.join(target_dir, f"{acc}.genome")

        # 1. Copy the FASTA, ROI, ALERTS file.
        shutil.copyfile(source_fasta, target_fasta)
        print(f"Copied FASTA from {source_fasta} to {target_fasta}")
        shutil.copyfile(source_alerts, target_alerts)
        print(f"Copied FASTA from {source_alerts} to {target_alerts}")

        # 2. Process the source GFF to ensure proper gene/transcript structure.
        process(source_gff, target_gff)
        print(f"Processed GFF file from {source_gff} to {target_gff}")

        # 3. Append the FASTA section to the processed GFF file.
        seq_section = ["##FASTA"]
        with open(target_fasta, "r") as handle:
            for line in handle:
                seq_section.append(line.strip())
        with open(target_gff, "a") as handle:
            handle.write("\n".join(seq_section) + "\n")
        print(f"Appended FASTA section from {target_fasta} to {target_gff}")

        # 4. Extract CDS and protein sequences from the processed GFF file.
        extract_cds_from_gff(target_gff, target_fasta, target_cds, target_protein, translation_table)

        # 5. Write the SNPeff configuration file.
        with open(target_conf, "w") as outf:
            outf.write(f"{acc}\n{acc}.genome : {acc}\n{acc}.chromosomes : {acc}\n")
        print(f"Wrote SNPeff configuration to {target_conf}")

        # (Optional) Create or touch any marker files required by your workflow.
