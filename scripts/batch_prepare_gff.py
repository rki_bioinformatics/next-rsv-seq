#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# author: Stephan Fuchs (Robert Koch Institute, FG13, fuchss@rki.de)

import csv
import os
import argparse

def generate_unique_id(existing_ids, base_id='gene'):
    count = 1
    new_id = f'{base_id}_{count}'
    while new_id in existing_ids:
        count += 1
        new_id = f'{base_id}_{count}'
    return new_id

def is_cds_covered_by_gene(cds_start, cds_end, cds_strand, gene_data):
    for gene_start, gene_end, gene_strand in gene_data:
        if cds_start >= gene_start and cds_end <= gene_end and cds_strand == gene_strand:
            return True
    return False

def add_missing_gene_ids(gff_file, output_file):
    existing_ids = set()
    gene_data = []  # List to store (start, end, strand) of genes
    entries_to_write = []

    with open(gff_file, 'r') as infile:
        reader = csv.reader(infile, delimiter='\t')

        # First pass to collect gene data
        for row in reader:
            if len(row) < 9 or row[0].startswith('#'):
                continue
            if row[2] == 'gene':
                gene_start = int(row[3])
                gene_end = int(row[4])
                gene_strand = row[6]
                gene_data.append((gene_start, gene_end, gene_strand))

        infile.seek(0)  # Reset file pointer to the beginning

        # Second pass to process CDS entries
        for row in reader:
            if len(row) < 9 or row[0].startswith('#'):   # Skip header and incomplete rows
                entries_to_write.append(row)
                continue

            if row[2] != 'gene' and row[2] != 'CDS':
                continue

            attributes = row[8].split(';')
            attr_dict = {attr.split('=')[0]: attr.split('=')[1] for attr in attributes if '=' in attr}

            if 'ID' not in attr_dict or attr_dict['ID'] == '':
                new_id = generate_unique_id(existing_ids, row[2])
                existing_ids.add(new_id)
                attr_dict['ID'] = new_id
                row[8] = ';'.join([f'{key}={value}' for key, value in attr_dict.items()])

            if row[2] == 'CDS' and not is_cds_covered_by_gene(int(row[3]), int(row[4]), row[6], gene_data):
                gene_id = generate_unique_id(existing_ids, 'gene')
                existing_ids.add(gene_id)
                gene_entry = row.copy()
                gene_entry[2] = 'gene'
                gene_entry[8] = f'ID={gene_id}'
                entries_to_write.append(gene_entry)  # Add the gene entry before current CDS

            entries_to_write.append(row)

    with open(output_file, 'w', newline='') as outfile:
        writer = csv.writer(outfile, delimiter='\t')
        for entry in entries_to_write:
            writer.writerow(entry)

def main():
    parser = argparse.ArgumentParser(description="Add missing gene IDs to a GFF file.")
    parser.add_argument("input_files", help="The path to the input GFF files", nargs="+")
    parser.add_argument("-d", help="output directory", required=True)
    parser.add_argument("-f", help="force overwriting", action="store_true")

    args = parser.parse_args()

    for input_file in args.input_files: 
        output_file = os.path.join(os.path.realpath(args.d), os.path.basename(os.path.realpath(input_file)))
        if not args.f and os.path.isfile(output_file):
            exit("error: " + output_file + " already exists.")
        add_missing_gene_ids(input_file, output_file)

if __name__ == "__main__":
    main()
