#!/usr/bin/env python3
import sys
import os
import re
import operator
from pathlib import Path
import pandas as pd

def parse_snpeff_vcf(vcf_file, min_alt_freq, min_total_dp):
    """Parses a SnpEff-annotated VCF file into a pandas DataFrame for filtering."""
    data = []
    with open(vcf_file, 'r') as file:
        for line in file:
            if line.startswith("#"):  # Skip header lines
                continue
            columns = line.strip().split("\t")
            chrom, pos, id_, ref, alt, qual, filt, info = columns[:8]            
            
            # Filter by PASSED  
            if filt == 'FALSE':
                continue
            
            # Filter by ALT-FREQ 
            freq_match = re.search(r'ALT_FREQ=([^;]+)', info)
            try:
                alt_freq = float(freq_match.group(1)) 
            except ValueError:
                raise ValueError(f"Incorrect ALT_FREQ value in '{vcf_file}': {freq_match.group(1)}")     
            if alt_freq < min_alt_freq:
                continue
 
             # Filter by TOTAL_DP 
            dp_match = re.search(r'TOTAL_DP=([^;]+)', info)
            try:
                total_dp = int(dp_match.group(1)) 
            except ValueError:
                raise ValueError(f"Incorrect TOTAL_DP value in '{vcf_file}': {total_dp.group(1)}")     
            if total_dp < min_total_dp:
                continue          
            
            # Extract ANN= field from INFO column.
            ann_match = re.search(r'ANN=([^;]+)', info)
            if not ann_match:
                continue  # Skip if no ANN field
            ann_field = ann_match.group(1)
            freq = freq_match.group(1)
            dp = dp_match.group(1)
            annotations = ann_field.split(",")  # Multiple effects possible
            for ann in annotations:
                fields = ann.split("|")
                if len(fields) < 15:  # Ensure we have enough fields
                    continue
                allele, annotation, impact, gene_name, gene_id, feature_type, feature_id, biotype, rank_total, \
                hgvs_c, hgvs_p, cdna_pos, cds_pos, aa_pos, distance = fields[:15]
                data.append({
                    "CHROM": chrom,
                    "POS": int(pos),
                    "REF": ref,
                    "ALT": allele,
                    "ALT_FREQ": float(alt_freq),
                    "TOTAL_DP": int(total_dp),
                    "PASS": filt,
                    "Gene": gene_name,
                    "Effect": annotation,
                    "Impact": impact,
                    "HGVS_c": hgvs_c,   # cDNA change
                    "HGVS_p": hgvs_p,   # Protein change in HGVS format
                    "AA_Pos": aa_pos,   # Amino acid position
                    "Region": feature_type,
                    "Biotype": biotype
                })                
    df = pd.DataFrame(data)
    return df


def format_variations(variation, gene=None, reverse=False):
    """
    Convert a protein variation between single-letter and three-letter (HGVS) formats.
    
    Ambiguous mutations (i.e. those containing 'X') are expanded into a list of all possible
    substitutions using a predefined list of standard amino acids.
    
    When reverse is False:
      - Expects a single-letter mutation (e.g., "L68Q" or "S451X").
      - Returns a list of strings in HGVS format (e.g., ["p.Leu68Gln"] or, for ambiguity,
        ["p.Ser451Ala", "p.Ser451Arg", …]).
    
    When reverse is True:
      - Expects a HGVS-style mutation starting with "p." (e.g., "p.Leu68Gln" or "p.Ser451Xaa").
      - Returns a list of strings in single-letter format, optionally prefixed with '<gene>:' 
        (e.g., ["L68Q"] or, for ambiguity, ["S451A", "S451R", …]).
    """
    # Mapping for unambiguous one-letter to three-letter codes.
    letter_code = {
        'A': 'Ala', 'R': 'Arg', 'N': 'Asn', 'D': 'Asp', 'C': 'Cys',
        'E': 'Glu', 'Q': 'Gln', 'G': 'Gly', 'H': 'His', 'I': 'Ile',
        'L': 'Leu', 'K': 'Lys', 'M': 'Met', 'F': 'Phe', 'P': 'Pro',
        'S': 'Ser', 'T': 'Thr', 'W': 'Trp', 'Y': 'Tyr', 'V': 'Val',
        'U': 'Sec', 'O': 'Pyl'
    }    
    if not reverse:
        # Expect input like "L68Q" (possibly with an ambiguous X)
        m = re.match(r"^([A-Z])([0-9]+)([A-Z])$", variation)
        if not m:
            return []
        wt = m.group(1)
        pos = m.group(2)
        mut = m.group(3)
        # Expand ambiguous letters.
        wt_list = [wt] if wt != 'X' else sorted(letter_code.keys())
        mut_list = [mut] if mut != 'X' else sorted(letter_code.keys())
        
        results = []
        for w_letter in wt_list:
            if w_letter not in letter_code:
                return []
            for m_letter in mut_list:
                if m_letter not in letter_code:
                    return []
                results.append("p." + letter_code[w_letter] + pos + letter_code[m_letter])
        return results
    else:
        # Reverse mapping for three-letter to one-letter.
        rev_letter_code = {v: k for k, v in letter_code.items()}
        
        # Reverse conversion: expect a HGVS string like "p.Leu68Gln" (or with an ambiguous "Xaa")
        if not variation.startswith("p."):
            return []
        body = variation[2:]
        m = re.match(r"^([A-Z][a-z]{2})([0-9]+)([A-Z][a-z]{2})$", body)
        if not m:
            return []
        wt_three = m.group(1)
        pos = m.group(2)
        mut_three = m.group(3)
        gene_prefix = f"{gene}:" if gene is not None else ""
        # For ambiguous three-letter codes we expect "Xaa"
        if wt_three == "Xaa":
            wt_list = sorted(letter_code.keys())
        else:
            if wt_three not in rev_letter_code:
                return []
            wt_list = [rev_letter_code[wt_three]]
        if mut_three == "Xaa":
            mut_list = possible_aa
        else:
            if mut_three not in rev_letter_code:
                return []
            mut_list = [rev_letter_code[mut_three]]
        results = []
        for w_letter in wt_list:
            for m_letter in mut_list:
                results.append(gene_prefix + w_letter + pos + m_letter)
        return results


def extract_position(variant):
    """
    Extracts the numeric position from a variant string.
    For example, given 'L68Q' or 'F:L68Q', it returns 68.
    """
    m = re.search(r'(\d+)', variant)
    if m:
        return int(m.group(1))
    return None

def count_effects(df, effect, min_alt_freq, min_total_dp, gene=None):

    if not gene is None:
        df = df[(df["Gene"] == gene)]

    df = df[(df["Effect"] == effect) &
            (df["ALT_FREQ"] >= min_alt_freq) &
            (df["TOTAL_DP"] >= min_total_dp)
           ]
    return len(df)

def main():
    # Snakemake inputs and parameters.
    ref_file    = snakemake.input.ref
    snpeff_vcf  = snakemake.input.snpeff_vcf
    datadir     = snakemake.params.datadir
    output      = snakemake.output
    sample      = snakemake.wildcards.sample
    alert_treshold    = float(snakemake.params.alert_treshold)
    moc_treshold    = float(snakemake.params.moc_treshold)
    min_coverage = int(snakemake.params.min_cov)
    
    # Read reference accession used for SNPEFF.
    ref_acc = Path(ref_file).read_text().strip()
    
    # Process ALERTS.
    alert_file = os.path.join(datadir, f"{ref_acc}.alerts")
    alerts = []
    with open(alert_file, "r") as handle:
        for i, line in enumerate(handle):
            if i == 0 or line.startswith("#") or line.strip() == "":
                continue
            alerts.append([x.strip() for x in line.split("\t")])
    
    # Screening: Parse the SnpEff VCF.
    snpeff_df = parse_snpeff_vcf(snpeff_vcf, alert_treshold, min_coverage)
        
    # Variables to hold aggregated alert info.
    # Each alert is stored as a tuple: (alert_level, alert_color, position, variant)
    present_alerts = []
    alert_levels = set()
    max_alert = (-1, "")  # Tuple: (max alert level, associated alert color)
    
    # Loop over alerts.
    # Expected columns in alerts: alert_color, alert_level, profile_type, profile_gene, profile_definition
    for alert_color, alert_level, profile_type, profile_gene, profile_definition in alerts:
        profile_type = profile_type.upper()
        alert_level = int(alert_level)
        alert_levels.add(alert_level)
        
        # GENE profile.
        if profile_type == "GENE":
            try:
                op, effect = profile_definition.split(" ")
            except ValueError:
                raise ValueError("Alert profile format incorrect  in '{ref_acc}.alerts': " + profile_definition)
            if op == "=":
                op_func = operator.eq
            elif op == "!=":
                op_func = operator.ne
            else:
                raise ValueError(f"Unsupported operator in '{ref_acc}.alerts': {op}")
                print("ALERT RESULT GENE", result)
                
            result = snpeff_df[(snpeff_df["Gene"] == profile_gene) & (op_func(snpeff_df["Effect"], effect))]
            for _, row in result.iterrows():
                # Convert HGVS mutation to single-letter format (which may yield multiple possibilities).
                variants = format_variations(row['HGVS_p'], gene=profile_gene, reverse=True)
                for variant in variants:
                    formatted_variant = f"{variant} ({round(row['ALT_FREQ'] * 100)}%)"
                    present_alerts.append((alert_level, alert_color, int(row['POS']), formatted_variant))
            if not result.empty and alert_level > max_alert[0]:
                max_alert = (alert_level, alert_color)
        
        # ROI profile.
        elif profile_type == "AA_ROI":
            try:
                start, end = profile_definition.split("-")
            except ValueError:
                raise ValueError(f"Incorrect ROI profile format in '{ref_acc}.alerts': {profile_definition}")   
            result = snpeff_df[(snpeff_df["Gene"] == profile_gene) &
                               (snpeff_df["POS"] >= int(start)) &
                               (snpeff_df["POS"] <= int(end))]
            for _, row in result.iterrows():
                variants = format_variations(row['HGVS_p'], gene=profile_gene, reverse=True)
                for variant in variants:
                    formatted_variant = f"{variant} ({round(row['ALT_FREQ'] * 100)}%)"
                    present_alerts.append((alert_level, alert_color, int(row['POS']), formatted_variant))
            if not result.empty and alert_level > max_alert[0]:
                max_alert = (alert_level, alert_color)                        
        
        # AA_CHANGE profile.
        elif profile_type == "AA_CHANGE":
            variants_str = profile_definition  # e.g., "L68X+L68R" or "L100P"
            for alert_variant in (v.strip() for v in variants_str.split(",")):
                comp_mutations = alert_variant.split("+")
                all_match = True
                comp_positions = []
                comp_frequencies = []
                gene_df = snpeff_df[snpeff_df["Gene"] == profile_gene]
                # Build a dictionary mapping HGVS_p to ALT_FREQ for quick lookup.
                mutation_freq = {row['HGVS_p']: row['ALT_FREQ'] for _, row in gene_df.iterrows()}
                present_mutations = set(gene_df["HGVS_p"])
                present_positions = {row['HGVS_p']: int(row['POS']) for _, row in gene_df.iterrows()}
                for comp in comp_mutations:
                    poss_list = format_variations(comp)
                    if not poss_list:
                        raise ValueError(f"Incorrect mutation definition format in '{ref_acc}.alerts': {comp}")
                    intersection = set(poss_list) & present_mutations
                    if not intersection:
                        all_match = False
                        break
                    else:
                        # Choose the matching mutation with the minimum position.
                        best_mut = min(intersection, key=lambda m: present_positions[m])
                        pos_val = present_positions[best_mut]
                        comp_positions.append(pos_val)
                        comp_frequencies.append(mutation_freq[best_mut])
                if all_match:
                    alert_pos = min(comp_positions) if comp_positions else 0
                    # Format each component with its corresponding frequency.
                    formatted_components = [
                        f"{profile_gene}:{comp} ({round(freq * 100)}%)"
                        for comp, freq in zip(comp_mutations, comp_frequencies)
                    ]
                    alert_variant_formatted = "+".join(formatted_components)
                    present_alerts.append((alert_level, alert_color, alert_pos, alert_variant_formatted))

        
        else:
            raise ValueError(f"Unknown profile type in '{ref_acc}.alerts': {profile_type}")
    
    # Determine maximum alert level from the set.
    max_value = max(alert_levels) if alert_levels else 0
    
    # Create an aggregated TSV output.
    # Heading: ALERT, ALERT LEVEL, and one column per alert level (from max_value down to 1)
    heading = ["ALERT", "ALERT LEVEL"]
    
    # Use the max_alert tuple for the top alert.
    if max_alert[0] >= 0:
        top_line = [max_alert[1], f"{max_alert[0]} of {max_value}"]
    else:
        top_line = ["none", "0 of {max_value}"]
    
    # For each alert level from max_value down to 1, gather unique variants sorted by position.
    seen_variants = set()
    for alevel in range(max_value, 0, -1):
        heading.append(f"MOC of ALERT LEVEL {alevel}")
        # Flatten variants in case some alerts represent multiple possibilities.
        variants_set = set()
        for tup in present_alerts:
            if tup[0] == alevel:
                if tup[3] not in seen_variants:
                    variants_set.add(tup[3])
                    seen_variants.add(tup[3])
                    seen_variants.update(tup[3].split("+"))
                
        mutations = sorted(variants_set, key=lambda v: extract_position(v))
        if variants_set:
          top_line.append(",".join(mutations))
        else:
          top_line.append("")
        
    # EFFECTS OF INTEREST (EOI)
    eois = ['missense_variant', 'stop_gained', 'conservative_inframe_insertion', 'disruptive_inframe_insertion', 'conservative_inframe_deletion', 'disruptive_inframe_deletion', 'frameshift_variant']
    for eoi in eois:
        heading.append(eoi)
        total_count = count_effects(snpeff_df, eoi, moc_treshold, min_coverage, gene=None)
        if total_count == 0:
            top_line.append("0")
            continue
        f_gene_count = count_effects(snpeff_df, eoi, moc_treshold, min_coverage, gene="F")
        top_line.append(f"{f_gene_count} (F) {total_count} (total)")
    
    # Write the aggregated output TSV.
    with open(output[0], "w") as handle:
        handle.write("\t".join(heading) + "\n" + "\t".join(top_line) + "\n")


if __name__ == "__main__":
    main()
