#!/usr/bin/env python3
"""
ivar_tsv_to_csv.py

This script converts an ivar TSV output file into a VCF file.
It is intended to be run via the Snakemake `script:` directive.
The script automatically uses:
  - snakemake.input.tsv : the input TSV file.
  - snakemake.output.vcf : the output VCF file.

This version adds header lines for contigs and FILTER definitions to
resolve warnings such as:
  [W::vcf_parse] Contig 'OP975389.1' is not defined in the header.
  [W::vcf_parse_filter] FILTER 'FALSE' is not defined in the header.
  [W::vcf_parse_filter] FILTER 'TRUE' is not defined in the header.
"""

import csv

# Get file paths from the snakemake object.
input_tsv = snakemake.input.tsv
output_vcf = snakemake.output.vcf

# Read the entire TSV to collect all rows and determine unique contigs.
with open(input_tsv, 'r') as tsv_file:
    reader = csv.DictReader(tsv_file, delimiter='\t')
    rows = list(reader)

# Collect unique contigs from the "REGION" field.
unique_contigs = set()
for row in rows:
    region = row.get("REGION", "").strip()
    if region:
        unique_contigs.add(region)
# If no contigs are found, add a default.
if not unique_contigs:
    unique_contigs.add("chrUnknown")

with open(output_vcf, 'w') as vcf_file:
    # Write standard VCF header lines.
    vcf_file.write("##fileformat=VCFv4.2\n")
    vcf_file.write("##source=ivar_tsv_to_csv.py\n")
    
    # Define INFO fields.
    vcf_file.write("##INFO=<ID=ALT_FREQ,Number=1,Type=Float,Description=\"Alternate allele frequency\">\n")
    vcf_file.write("##INFO=<ID=TOTAL_DP,Number=1,Type=Integer,Description=\"Total read depth\">\n")
    
    # Define FILTER fields.
    vcf_file.write("##FILTER=<ID=TRUE,Description=\"Variant passed all filters\">\n")
    vcf_file.write("##FILTER=<ID=FALSE,Description=\"Variant did not pass filters\">\n")
    
    # Write contig header lines.
    # (The length attribute is set to 0 as a placeholder.)
    for contig in sorted(unique_contigs):
        vcf_file.write(f"##contig=<ID={contig},length=0>\n")
    
    # Write the column header line.
    vcf_file.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n")
    
    # Process each row from the TSV.
    for row in rows:
        # Use the REGION field as the chromosome; if missing, use a default.
        chrom = row.get("REGION", "chrUnknown").strip() or "chrUnknown"
        pos = row.get("POS")
        if pos is None or pos == "":
            continue  # Skip rows without a position.
        vid = "."
        ref = row.get("REF", ".")
        alt = row.get("ALT", ".")
        qual = "."
        # Use the value from the PASS field; default to "PASS" if not provided.
        filter_val = row.get("PASS", "PASS")
        
        # Build the INFO field from ALT_FREQ and TOTAL_DP (if available).
        info_items = []
        alt_freq = row.get("ALT_FREQ")
        if alt_freq not in (None, ""):
            info_items.append(f"ALT_FREQ={alt_freq}")
        total_dp = row.get("TOTAL_DP")
        if total_dp not in (None, ""):
            info_items.append(f"TOTAL_DP={total_dp}")
        info = ";".join(info_items) if info_items else "."
        
        # Write the VCF record.
        vcf_file.write(f"{chrom}\t{pos}\t{vid}\t{ref}\t{alt}\t{qual}\t{filter_val}\t{info}\n")
