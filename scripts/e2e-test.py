#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author: Stephan Fuchs (Robert Koch Institute, FG13, fuchss@rki.de)

VERSION = "0.0.9"
import os
import argparse
import re
from Bio import SeqIO, pairwise2
import sys
import random
import time
import gzip
import subprocess
import tempfile
import shutil

def parse_args():
	parser = argparse.ArgumentParser(prog="sim_amplicon.py", description="", )
	parser.add_argument('fasta', metavar='FASTA_FILE', help="reference genome sequence in FASTA format", type=str)
	parser.add_argument('-s', '--snp', metavar='STR', help="range of snp number (default: 50-150)", type=str, default="50-150")
	parser.add_argument('-i', '--ins', metavar='STR', help="range of insertion number (default: 1-7)", type=str, default="1-7")
	parser.add_argument('-g', '--gap', metavar='STR', help="range of del number (default: 1-10)", type=str, default="1-10")
	parser.add_argument('-l', '--len', metavar='STR', help="range of indel lengths (default: 1-10)", type=str, default="1-10")
	parser.add_argument('-d', '--depth', metavar='INT', help="designated avg read depth, log-normal distributed (default: 1000)", type=int, default=1000)
	parser.add_argument('-p', '--platform', metavar='STR', help="sequencing platform to simulate (default: random)", choices=['MiSeq', 'HiSeq', 'NovaSeq', 'random'], default="random")
	parser.add_argument('-r', '--repeats', metavar='INT', help="test repetitions (default: 1)", type=int, default=1)
	parser.add_argument('--dir', metavar='DIR', help="working dir (default: temporary dir)", type=str, default=None)
	parser.add_argument('--threads', metavar='INT', help="cpus to use (default: 1)", type=int, default=1)
	parser.add_argument('--version', action='version', version='%(prog)s ' + VERSION)
	return parser.parse_args()

def read_fasta(fname):
	record = SeqIO.read(fname, "fasta")
	return record.id, str(record.seq)

def add_snp(seqlist, pos):
	nucs = "ATGC"
	snp = random.choice(list(nucs.replace(seqlist[pos], "")))
	seqlist[pos] = snp
	return seqlist

def add_ins(seqlist, pos, length):
	nucs = list("ATGC")
	ins = "".join([random.choice(nucs) for x in range(length)])
	seqlist[pos] += ins
	return seqlist

def add_del(seqlist, pos, length):
	for x in range(length):
		seqlist[pos] = ""
		pos += 1
	return seqlist

def range_from_args(input):
	elems = input.split("-")
	return range(int(elems[0]), int(elems[-1])+1)

def simseq(template_seq, snp_count, ins_count, del_count, len_range, out_file):
	seqlist = list(template_seq)
	varpos = random.choices(range(len(seqlist)), k=snp_count+ins_count+del_count)
	# add snps
	for _ in range(snp_count):
		seqlist = add_snp(seqlist, varpos.pop())
	# add insertions
	for _ in range(ins_count):
		seqlist = add_ins(seqlist, varpos.pop(), random.choice(len_range))
	# add deletion
	for _ in range(del_count):
		seqlist = add_del(seqlist, varpos.pop(), random.choice(len_range))
	# write fasta
	with open(out_file, "w") as handle:
		handle.write(">simseq\n" + "".join(seqlist))

def simreads(fasta_in, prefix, platform, read_count, cpus):
	cmd = ["iss", "generate", "--genomes", os.path.realpath(fasta_in), "--model", platform, "-n", read_count, "-z", "-o", prefix, "-p", cpus]
	subprocess.Popen([str(x) for x in cmd]).wait()

def cwd(args):
	if not args.dir:
		return tempfile.TemporaryDirectory()
	elif not os.path.exists(args.dir):
		os.makedirs(args.dir)
	return os.path.abspath(args.dir)

def run_test(args):
	acc, seq = read_fasta(args.fasta)
	seqlen = len(seq)
	snp_count = random.choice(range_from_args(args.snp))
	ins_count = random.choice(range_from_args(args.ins))
	del_count = random.choice(range_from_args(args.gap))
	len_range = range_from_args(args.len)
	if args.platform == "random":
		args.platform = random.choice(['MiSeq', 'HiSeq', 'NovaSeq'])
	if args.platform == 'MiSeq':
		read_count = int((seqlen * args.depth)/(250*2))
	else:
		read_count = int((seqlen * args.depth)/(150*2))
	dirname = cwd(args)
	for r in range(args.repeats):
		msg = "TEST RUN " + str(r+1) + " OF " + str(args.repeats)
		print(msg)
		print("="*len(msg))
		print('temporary directory:', dirname)
		print('template:...........', acc, "(" + str(len(seq)) + " bp)")
		print('snp count:..........', snp_count)
		print('ins count:..........', ins_count)
		print('del count:..........', del_count)
		print('indel length:.......', min(len_range), "bp -", max(len_range), "bp")
		print('platform profile:...', args.platform)
		print('sequencing depth:...', args.depth)
		print('read count:.........', read_count)
		sim_fasta = os.path.join(dirname, "sim.fasta")
		fq_prefix = os.path.join(dirname, "simdat")
		simseq(seq, snp_count, ins_count, del_count, len_range, sim_fasta)
		simreads(sim_fasta, fq_prefix, args.platform, read_count, args.threads)
		rsvpipe = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "rsvpipe.py"))
		cmd = [rsvpipe, "-f", os.path.join(dirname, "simdat_R1.fastq.gz"), os.path.join(dirname, "simdat_R2.fastq.gz"), "-o", dirname, "--tag", "_R1", "_R2", "--threads", args.threads]
		subprocess.Popen([str(x) for x in cmd]).wait()
		recovered_fasta = os.path.join(dirname, "results", "consensus", "simdat.fa")
		if not compare_seq(sim_fasta, recovered_fasta):
			with open(os.path.join(dirname, "seqs2algn.fasta"), "w") as outfile:
				for fname in [args.fasta, sim_fasta, recovered_fasta]:
					with open(fname) as infile:
						outfile.write(infile.read() + "\n")
			sys.exit(msg + " FAILED")
		else:
			print(msg + " SUCCESSFULL")
	shutil.rmtree(dirname)


def compare_seq(orig_fasta, recovered_fasta, show_max_diffs = 5):
	id1, seq1 = read_fasta(orig_fasta)
	id2, seq2 = read_fasta(recovered_fasta)
	return seq2.strip("-N") in seq1

def get_cmd_from_snake(snakemake):
    cmd = ["tsv", snakemake.input['tsv'],
          "fasta", snakemake.input['ref'],
		  "pileup", snakemake.input['pileup'],
		  "--outfile", snakemake.input['output'],
		  "--log", snakemake.log,
		  "--id", snakemake.params[''],
		  ]
    cmd = [str(arg) for arg in cmd]
    return cmd


def main():
	args = parse_args()
	run_test(args)


if __name__ == "__main__":
	main()
