rule createConsensus:
	input:
 		pileup = os.path.join(DATAFOLDER["pileup"], "{sample}.pileup.gz"),
		variant = os.path.join(DATAFOLDER["variant_calling"], "{sample}.tsv"),
		reference = REFERENCE
	output:
		os.path.join(CNSFOLDER, "{sample}.fa")
	params:
		base_minqual = BASE_MINQUAL,
		cns_minfreq = CNS_MINFREQ_CUSTOM,
		cns_mincov = CNS_MINCOV,
		cns_mincount = CNS_MINCOUNT,
		cns_maxpval = CNS_MAXPVAL
	conda:
		"../envs/pandas.yaml"
	log:
		os.path.join(DATAFOLDER["logs"], "masking", "{sample}.consensus_mask.log")
	script:
		"../scripts/create_consensus.py"
