rule prepareFASTA:
    input:
        csv=os.path.join(DATAFOLDER["references"], "{sample}.csv")
    output:
        fasta=os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta"),
        gff=os.path.join(DATAFOLDER["references"], "ref_for_{sample}.gff")
    conda:
        "../envs/biopython.yaml"
    params:
        fasta_script=os.path.join(os.path.dirname(workflow.snakefile), "..", "scripts", "dnasafe_fasta.py"),
        gff_script=os.path.join(os.path.dirname(workflow.snakefile), "..", "scripts", "prepare_gff.py")    
    shell:
        """
        # Extract acession, fasta and gff filenames from the csv file
        read acc fasta_file gff_file <<< $(gawk -F'\t' 'NR==1 {{print $1, $2, $3}}' {input.csv})

        # Call your Python script with the fasta file
        {params.fasta_script} --fasta "$fasta_file" --out {output.fasta} --acc "$acc"

        # Copy the gff file to the destination
        {params.gff_script} "$gff_file" {output.gff}
        """
