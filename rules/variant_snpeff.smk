rule prepare_snpeff_db:
    input:
        # List all FASTA and GFF files based on sample names.
        fastas = expand(os.path.join(SNPEFF_REF_DIR, "{acc}.fasta"), acc=SNPEFF_ACCESSIONS),
        gff = expand(os.path.join(SNPEFF_REF_DIR, "{acc}.gff3"), acc=SNPEFF_ACCESSIONS),
        alert = expand(os.path.join(SNPEFF_REF_DIR, "{acc}.alerts"), acc=SNPEFF_ACCESSIONS)
    output:
        conf = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}.genome"), acc=SNPEFF_ACCESSIONS),
        fasta = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}", "sequences.fasta"), acc=SNPEFF_ACCESSIONS),
        gff = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}", "genes.gff"), acc=SNPEFF_ACCESSIONS),
        cds = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}", "cds.fa"), acc=SNPEFF_ACCESSIONS),
        protein = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}", "protein.fa"), acc=SNPEFF_ACCESSIONS),
        alert = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}.alerts"), acc=SNPEFF_ACCESSIONS)
    params:
        ttable=1,
        ref_dir=SNPEFF_REF_DIR,
        out_dir=os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb"),
        accs=SNPEFF_ACCESSIONS
    conda:
        "../envs/bcbio.yaml"
    script:
        "../scripts/prepare_snpeff_db.py"
            
rule build_snpeff_db:
    """
    Build a SNPeff database for a given accession.
    The rule uses a configuration file for the accession and produces two output files:
      - sequence.bin
      - snpEffectPredictor.bin
    """
    input:
        conf = os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}.genome")
    output:
        seq_bin = os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}", "sequence.bin"),
        pred_bin = os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}", "snpEffectPredictor.bin")
    conda:
        "../envs/snpeff.yaml"
    params:
        datadir = os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb")
    log:
        os.path.join(DATAFOLDER["logs"], "gene_inspection", "{acc}.snpeff_db.log")
    shell:
        r"""
        # Build the SNPeff database for the accession {wildcards.acc}
        snpEff build -gff3 -c {input.conf} -datadir {params.datadir} {wildcards.acc} > {log} 2>&1
        """

rule get_snpeff_ref:
    input:
        query=os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta"),
        candidates = expand(os.path.join(SNPEFF_REF_DIR, "{acc}.fasta"), acc=SNPEFF_ACCESSIONS)
    output:
        os.path.join(DATAFOLDER["gene_inspection"], "{sample}.snpeff.ref")
    conda:
		    "../envs/parasail.yaml"
    script:
        "../scripts/get_best_ref.py"
        
rule generate_chain_file:
    """
    Generate a chain file to convert coordinates from RefA to RefB using UCSC tools.
    This rule aligns RefA (input.ref_a) against RefB (from the snpeff database for the sample)
    using lastz to generate an AXT file, converts the AXT file to a chain file with axtChain,
    and then sorts/merges the chain with chainMergeSort.
    """
    input:
        ref_a = os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta"),
        snpeff_ref = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.snpeff.ref")
    output:
        chain = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.chain")
    params:
        datadir = os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb")
    conda:
        "../envs/ucsc.yaml"
    log:
        os.path.join(DATAFOLDER["logs"], "gene_inspection", "{sample}.chaining.log")           
    shell:
        r"""
        # Read the accession from the snpeff reference file
        acc=$(cat {input.snpeff_ref})
        # Align RefA to RefB (sequences.fasta for the given accession) using lastz,
        # and output in AXT format.
        lastz {input.ref_a} {params.datadir}/${{acc}}/sequences.fasta --format=axt > {output.chain}.axt
        # Convert the AXT alignment to a chain file.
        axtChain -linearGap=medium -faT -faQ {output.chain}.axt {input.ref_a} {params.datadir}/${{acc}}/sequences.fasta {output.chain}.chain
        # Merge and sort the chain file.
        chainMergeSort {output.chain}.chain > {output.chain}.sorted.chain
        # Replace the unsorted chain file with the sorted version.
        mv {output.chain}.sorted.chain {output.chain}
        """

rule extend_variant_tsv:
    """Lift over VCF files from RefA to RefB using CrossMap"""
    input:
        tsv = os.path.join(DATAFOLDER["variant_calling"], "{sample}.tsv"),
        pileup = os.path.join(DATAFOLDER["pileup"], "{sample}.pileup.gz")
    output:
        tsv = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.extended.tsv")
    params:
        mincov = CNS_MINCOV
    conda:
        "../envs/python.yaml"       
    script:
        "../scripts/extend_vcf.py"

rule extended_tsv_to_vcf:
    input:
        tsv=os.path.join(DATAFOLDER["gene_inspection"], "{sample}.extended.tsv")
    output:
        vcf=os.path.join(DATAFOLDER["gene_inspection"], "{sample}.extended.vcf")
    conda:
        "../envs/python.yaml"
    script:
        "../scripts/ivar_tsv_to_vcf.py"

rule crossmap_vcf:
    """Lift over VCF files from RefA to RefB using CrossMap"""
    input:
        vcf=os.path.join(DATAFOLDER["gene_inspection"], "{sample}.extended.vcf"),
        chain=os.path.join(DATAFOLDER["gene_inspection"], "{sample}.chain"),
        snpeff_ref=os.path.join(DATAFOLDER["gene_inspection"], "{sample}.snpeff.ref")
    output:
        vcf=os.path.join(DATAFOLDER["gene_inspection"], "{sample}.crossmapped.vcf")
    params:
        datadir = os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb")
    conda:
        "../envs/crossmap.yaml"
    log:
        os.path.join(DATAFOLDER["logs"], "gene_inspection", "{sample}.crossmapping.log")        
    shell:
        r"""
        acc=$(cat {input.snpeff_ref})
        CrossMap vcf "{input.chain}" "{input.vcf}" "{params.datadir}/${{acc}}/sequences.fasta" "{output.vcf}"
        """

rule run_snpeff:
    input:
        vcf = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.crossmapped.vcf"),
        seq_bin = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}", "sequence.bin"), acc=SNPEFF_ACCESSIONS),
        pred_bin = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}", "snpEffectPredictor.bin"), acc=SNPEFF_ACCESSIONS),
        ref = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.snpeff.ref")
    output:
        ann_vcf = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.ann.vcf")
    params:
        datadir = os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb")
    log:
        os.path.join(DATAFOLDER["logs"], "gene_inspection", "{sample}.snpeff.log")            
    conda:
        "../envs/snpeff.yaml"
    shell:
        r"""
        acc=$(cat {input.ref})
        snpEff -c "{params.datadir}/${{acc}}.genome" -ud 50 -dataDir {params.datadir} -v "${{acc}}" {input.vcf} > {output.ann_vcf}
        """