rule getInsertSize:
    input:
        bam = os.path.join(DATAFOLDER["mapping"], "{sample}.sorted.bam")
    output:
        sizes = os.path.join(DATAFOLDER["mapping_stats"], "{sample}.fragsize.tsv")
    log:
        os.path.join(DATAFOLDER["logs"], "mapping_stats", "{sample}.fragsize.log")
    conda:
        "../envs/samtools.yaml"
    shell:
        r"""
            echo 0 1> {output.sizes}
            samtools view -F 4 {input.bam} | cut -f9 1>> {output.sizes} 2> {log}
        """
