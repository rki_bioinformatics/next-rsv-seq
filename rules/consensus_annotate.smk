rule annotateConsensus:
	input:
		fasta = os.path.join(CNSFOLDER, "{sample}.fa"),
		gff = os.path.join(DATAFOLDER["references"], "ref_for_{sample}.gff"),
		ref = os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta")
	output:
		gff = os.path.join(CNSFOLDER, "{sample}.gff"),
		mmi = temp(os.path.join(CNSFOLDER, "{sample}.fa.mmi")),
		fai = temp(os.path.join(CNSFOLDER, "{sample}.fa.fai"))
	log:
		os.path.join(DATAFOLDER["logs"], "annotation", "{sample}.log")
	params:
		path = os.path.join(DATAFOLDER["annotation"], "{sample}_liftoff")
	conda:
		"../envs/liftoff.yaml"
	threads: 1
	shell:
		r"""
			mkdir -p "{params.path}"
			pushd "{params.path}"
			liftoff -o {output.gff} -dir "{params.path}" -g {input.gff} {input.fasta} {input.ref}  &> {log}
			popd
		"""
