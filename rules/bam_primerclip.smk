def input_clipPrimer(wildcards):
	files = {}
	if READ_DEDUP:
		files['bam'] = os.path.join(DATAFOLDER["mapping"], "{sample}.dedupped.bam")
	else:
		files['bam'] = os.path.join(DATAFOLDER["mapping"], "{sample}.sorted.bam")
	if PRIMER:
		files['bed'] = PRIMER
	return files


rule clipPrimer:
	input:
		unpack(input_clipPrimer)
	output:
		bam = temp(os.path.join(DATAFOLDER["mapping"], "{sample}.clipped.bam"))
	log:
		os.path.join(DATAFOLDER["logs"], "mapping", "{sample}.clipping.log")
	params:
		primer_opt = " -b " + PRIMER if PRIMER else "",
		prefix = os.path.join(DATAFOLDER["mapping"], "{sample}", "{sample}.primerclipped")
	conda:
		"../envs/ivar.yaml"
	shell:
		r"""
			ivar trim -i {input.bam}{params.primer_opt} -p {params.prefix} -m 0 -q 0 &> {log}
		"""
