rule trimReads:
	input:
		getFastq
	output:
		PE1 = temp(os.path.join(DATAFOLDER["trimmed"], "{sample}.R1.fastq.gz")),
		PE2 = temp(os.path.join(DATAFOLDER["trimmed"], "{sample}.R2.fastq.gz")),
		SE1 = temp(os.path.join(DATAFOLDER["trimmed"], "{sample}.SE.R1.fastq.gz")),
		SE2 = temp(os.path.join(DATAFOLDER["trimmed"], "{sample}.SE.R2.fastq.gz")),
		json = os.path.join(DATAFOLDER["trimmed"], "{sample}.fastp.json"),
		html = os.path.join(DATAFOLDER["trimmed"], "{sample}.fastp.html")
	log:
		os.path.join(DATAFOLDER["logs"], "trimming", "{sample}.log")
	params:
		adapters = "--adapter_fasta " + ADAPTERS if ADAPTERS else "",
		reqlen = "--length_required {}".format(READ_MINLEN) if READ_MINLEN != -1 else "",
		qualfilter = "--qualified_quality_phred {}".format(READ_MINQUAL)
	conda:
		"../envs/fastp.yaml"
	threads:
		1
	shell:
		r"""
			(
				conda list -p $CONDA_PREFIX | grep fastp
				set -x
				fastp \
					--in1 {input[0]} \
					--out1 {output.PE1} \
					--in2 {input[1]} \
					--out2 {output.PE2} \
					--unpaired1 {output.SE1} \
					--unpaired2 {output.SE2} \
					--json {output.json} \
					--html {output.html} \
				{params.adapters} \
				{params.qualfilter} \
				{params.reqlen} \
				--low_complexity_filter \
				--overrepresentation_analysis \
				--thread {threads} || {{
					ret=$?
					cp {log} $( sed -E 's/(.+).log/\1.err.log/' <<< {log} )
					exit $ret
				}}
				set +x
			) &> {log}
		"""
