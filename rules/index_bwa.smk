rule indexBwa:
    input:
        "{fname}"
    output:
        "{fname}.bwt.2bit.64"
    conda:
        "../envs/bwa.yaml"
    shell:
        r"""
            bwa-mem2 index {input} &> /dev/null
        """
