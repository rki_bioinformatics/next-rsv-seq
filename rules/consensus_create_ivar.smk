rule createConsensus:
	input:
 		pileup = os.path.join(DATAFOLDER["pileup"], "{sample}.pileup.gz")
	output:
		expand(os.path.join(CNSFOLDER, "{{sample}}.{ext}"), ext=['fa', 'qual.txt'])
	params:
		base_minqual = BASE_MINQUAL,
		cns_minfreq = CNS_MINFREQ_IVAR,
		cns_mincov = CNS_MINCOV,
		prefix = os.path.join(CNSFOLDER, "{sample}"),
	conda:
		"../envs/ivar.yaml"
	log:
		os.path.join(DATAFOLDER["logs"], "masking", "{sample}.consensus_mask.log")
	shell:
		r"""
			zcat {input.pileup} | ivar consensus -p {params.prefix} -q {params.base_minqual} -t {params.cns_minfreq} -m {params.cns_mincov} -i {wildcards.sample} > {log} 2>&1
		"""
