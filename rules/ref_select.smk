def input_setKmaRef(wildcards):
    if KMA:
        return [os.path.join(DATAFOLDER["kma"], "{sample}.kma.spa")]
    else:
        return [SAMPLES[wildcards.sample]["ref_fasta"], SAMPLES[wildcards.sample]["ref_gff"]]

rule createKmaDb:
    input:
        KMA_DB_FASTAS
    output:
        temp(expand(str(KMA_DB) + ".{ext}", ext=KMA_DB_EXT))
    conda:
        "../envs/kma.yaml"
    params:
        fastas = KMA_DB_FASTAS,
    log: 
        os.path.join(DATAFOLDER["logs"], "kma", "db_indexing.log")
    shell:
        """
        kma index -i {input} -o {KMA_DB} -Sparse  &> {log}
        """

rule runKma:
    input:
        unpack(input_fastq2map),
        expand(str(KMA_DB) + ".{ext}", ext=KMA_DB_EXT)
    output:
        os.path.join(DATAFOLDER["kma"], "{sample}.kma.spa") if KMA else "{sample}.spa"
    conda:
        "../envs/kma.yaml"
    params:
        base = os.path.join(DATAFOLDER["kma"], "{sample}") if KMA else "{sample}",
    log: 
        os.path.join(DATAFOLDER["logs"], "kma", "{sample}.kma.log")
    wildcard_constraints:
        sample="[^/]+"
    shell:
        "kma -ipe {input.PE1} {input.PE2} -o {params.base}.kma -t_db {KMA_DB} -1t1 -t 3 -mrs 0.9 -mem_mode -mq 50 -and -Sparse &> {log}"

rule setKmaRef:
    input:
        unpack(input_setKmaRef)
    output:
        os.path.join(DATAFOLDER["references"], "{sample}.csv")
    run:
        if KMA: 
            with open(input[0], "r") as handle:
                lines = handle.readlines()
                if len(lines) < 2:
                    acc = config["fallback_ref"] + "(fallback)"
                else:
                    acc = lines[1].split("\t")[0]
                    fname = acc.split(".")[0]
            SAMPLES[wildcards.sample]["ref_fasta"] = os.path.join(KMA_DB_FASTA_DIR, fname + ".fasta")
            SAMPLES[wildcards.sample]["ref_gff"] = os.path.join(KMA_DB_FASTA_DIR, fname+ ".gff")
        else:
            acc = get_ref_accession(SAMPLES[wildcards.sample]["ref_fasta"], SAMPLES[wildcards.sample]["ref_gff"])
        with open(output[0], "w") as handle:
            handle.write(acc + "\t" + SAMPLES[wildcards.sample]["ref_fasta"] + "\t" + SAMPLES[wildcards.sample]["ref_gff"])
