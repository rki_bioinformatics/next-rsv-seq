rule create_alerts:
    input:
        ref = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.snpeff.ref"),
        snpeff_vcf = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.ann.vcf"),
        alert = expand(os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb", "{acc}.alerts"), acc=SNPEFF_ACCESSIONS)
    output:
        os.path.join(DATAFOLDER["alerts"], "{sample}.alerts.csv")
    params:
        datadir = os.path.join(DATAFOLDER["gene_inspection"], "snpeffdb"),
        alert_treshold = config['alert_treshold'],
        moc_treshold = config['moc_treshold'],
        min_cov = config['cns_mincov']         
    script:
        "../scripts/create_alerts.py"
