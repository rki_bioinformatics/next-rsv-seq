rule indexPicard:
    input:
        os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta") 
    output:
        os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta") + PICARD_INDEX_EXT
    conda:
        "../envs/gatk.yaml"
    log:
        os.path.join(PROJFOLDER, "logs", "index", "picard_ref_for_{sample}.log")
    shell:
        r"""
            gatk CreateSequenceDictionary -R {input} &> {log}
        """
