def input_map2reference(wildcards):
    ref = os.path.join(DATAFOLDER["references"], "ref_for_" + wildcards.sample + ".fasta")
    return {"ref": ref, "ref_index": ref + BWA_INDEX_EXT}

rule map2reference:
    # add read group tags: https://gatkforums.broadinstitute.org/gatk/discussion/6472/read-groups
    input:
        unpack(input_fastq2map),
        unpack(input_map2reference)
    output:
        temp(os.path.join(DATAFOLDER["mapping"], "{sample,[^/]+}.genuine.bam"))
    log:
        os.path.join(DATAFOLDER["logs"], "mapping", "{sample}.log")
    conda:
        "../envs/bwa.yaml"
    threads:
        10
    shell:
        r"""
            (   time \
                bwa-mem2 mem -M -t {threads} \
                    -R '@RG\tID:{wildcards.sample}\tPU:{wildcards.sample}\tSM:{wildcards.sample}\tPL:ILLUMINA\tLB:000' \
                    {input.ref} \
                    {input.PE1} {input.PE2} | \
                    samtools view -Sb -@ {threads} -o {output} \
            ) &> {log}
        """
