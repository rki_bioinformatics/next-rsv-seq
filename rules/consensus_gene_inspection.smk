rule filter_fgene_variants:
    """
    Filter variants within specified gene coordinates and classify them as minor or main.
    Outputs a CSV file with intuitive headers.
    """
    input:
        ivar_tsv = os.path.join(DATAFOLDER["variant_calling"], "{sample}.tsv"),
        snpeff_vcf = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.ann.vcf"),
        snpeff_ref = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.snpeff.ref"),
        chain = os.path.join(DATAFOLDER["gene_inspection"], "{sample}.chain")
    output:
        csv = os.path.join(CNSFOLDER, "{sample}_variant_annotation.csv")
    params:
        gff = os.path.join(CNSFOLDER, "{sample}.gff"),
        gene_name = "F",
        minor_af_threshold = 0.5,
        main_af_threshold = 0.5,
        alert_treshold = config['alert_treshold'],
        moc_treshold = config['moc_treshold'],
        min_cov = config['cns_mincov'] 
    conda:
        "../envs/pandas.yaml"  # Ensure pandas is available
    script:
        "../scripts/filter_gene_variants.py"