rule sortBam:
	input:
		os.path.join(DATAFOLDER["mapping"], "{sample}.genuine.bam")
	output:
		temp(os.path.join(DATAFOLDER["mapping"], "{sample}.sorted.bam"))
	log:
		os.path.join(DATAFOLDER["logs"], "mapping", "{sample}.sorting.log")
	conda:
		"../envs/samtools.yaml"
	threads:
		10
	shell:
		r"""
			samtools sort -@ {threads} -o {output} {input} &> {log}
		"""
