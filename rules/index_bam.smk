rule indexMapping:
    input:
        "{path}/{fname}.bam"
    output:
        temp("{path}/{fname}.bam.bai")
    conda:
        "../envs/samtools.yaml"
    shell:
        r"""
            samtools index {input}
        """
