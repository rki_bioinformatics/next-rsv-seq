rule indexSamtools:
    input:
        "{fname}"
    output:
        "{fname}.fai"
    conda:
        "../envs/samtools.yaml"
    shell:
        r"""
            samtools faidx {input}
        """
