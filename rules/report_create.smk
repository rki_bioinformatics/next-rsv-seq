def input_create_report(wildcards):
    files = []
    files.extend([SAMPLES[x]["fwd"] for x in SAMPLES])
    files.extend([os.path.join(DATAFOLDER["trimmed"], x + ".R1.fastq.gz") for x in SAMPLES])
    files.extend([os.path.join(DATAFOLDER["mapping_stats"], x + ".genuine.bam.bamstats.txt") for x in SAMPLES])
    if READ_DEDUP:
        files.extend([os.path.join(DATAFOLDER["mapping_stats"], x + ".dedupped.bam.bamstats.txt") for x in SAMPLES])
    if PRIMER:
        files.extend([os.path.join(DATAFOLDER["mapping_stats"], x + ".clipped.bam.bamstats.txt") for x in SAMPLES])
    files.extend([os.path.join(DATAFOLDER["mapping_stats"], x + ".filtered.bam.bamstats.txt") for x in SAMPLES])
    files.extend([os.path.join(DATAFOLDER["mapping_stats"], x + ".coverage.tsv") for x in SAMPLES])
    files.extend([os.path.join(DATAFOLDER["references"], x + ".csv") for x in SAMPLES])
    if not NO_ALERT:
        files.extend([os.path.join(DATAFOLDER["alerts"], f"{x}.alerts.csv") for x in SAMPLES])
        files.extend([os.path.join(DATAFOLDER["gene_inspection"], f"{x}.snpeff.ref") for x in SAMPLES])
    return files

rule getBamStats:
    input:
        os.path.join(DATAFOLDER["mapping"], "{fname}")
    output:
        stats = os.path.join(DATAFOLDER["mapping_stats"], "{fname}.bamstats.txt"),
        stats_forR = os.path.join(DATAFOLDER["mapping_stats"], "{fname}.bamstats.pipe.txt")
    log:
        os.path.join(DATAFOLDER["logs"], "mapping_stats", "{fname}.bamstats.log")
    conda:
        "../envs/samtools.yaml"
    shell:
        r"""
            samtools flagstat {input} 1> {output.stats} 2> {log};
            cat {output.stats} | sed -e 's/ + /|/' | sed -e 's/ /|/' 1> {output.stats_forR} 2> {log}
        """
rule create_report:
    input:
        input_create_report
    output:
        os.path.join(PROJFOLDER, "summary.csv")
    params:
        cns_mincov = CNS_MINCOV
    run:
        def format_number(x, digits=2, factor=100, outtype="str"):
            x = round(x * factor, digits)
            return str(x) if outtype == "str" else x

        # Dummy format_percent function; adjust as needed
        def format_percent(value, digits=2):
            return f"{round(value * 100, digits)}%"

        rows = []
        target_line = 8

        for sample in SAMPLES:
            row = OrderedDict()
            row["sample"] = sample

            # alerts
            if not NO_ALERT:
                with open(os.path.join(DATAFOLDER["alerts"], f"{sample}.alerts.csv")) as handle:
                    reader = csv.DictReader(handle, delimiter='\t')
                    # Assuming you want to merge the first row of the alerts file into row
                    row.update(next(reader))
            
            # alert reference:
            if not NO_ALERT:
                with open(os.path.join(DATAFOLDER["gene_inspection"], f"{sample}.snpeff.ref")) as handle:
                    acc = handle.readline().strip()
                row['alert_reference'] = acc  

            # mapping reference:
            with open(os.path.join(DATAFOLDER["references"], sample + ".csv")) as handle:
                acc = handle.readline().split("\t")[0]
            row['mapping_reference'] = acc  

            # genuine read count
            fname = SAMPLES[sample]["fwd"]
            with gzip.open(fname, "rt") as inhandle:
                x = int(sum(1 for _ in inhandle) / 4) * 2
            row['raw_read_count'] = str(x)

            # trimmed read count
            fname = os.path.join(DATAFOLDER["trimmed"], sample + ".R1.fastq.gz")
            with gzip.open(fname, "rt") as inhandle:
                x = int(sum(1 for _ in inhandle) / 4) * 2
            row['trimmed_read_count'] = str(x)

            # mapped read count
            fname = os.path.join(DATAFOLDER["mapping_stats"], sample + ".genuine.bam.bamstats.txt")
            with open(fname, "r") as inhandle:
                for _ in range(target_line):
                    line = inhandle.readline()
                count = int(line.split()[0])
            row['mapped_read_count'] = str(count)

            # dedupped read count
            if READ_DEDUP:
                fname = os.path.join(DATAFOLDER["mapping_stats"], sample + ".dedupped.bam.bamstats.txt")
                with open(fname, "r") as inhandle:
                    for _ in range(target_line):
                        line = inhandle.readline()
                count = int(line.split()[0])
                row['dedupped_mapped_read_count'] = str(count)

            # clipped read count
            if PRIMER:
                fname = os.path.join(DATAFOLDER["mapping_stats"], sample + ".clipped.bam.bamstats.txt")
                with open(fname, "r") as inhandle:
                    for _ in range(target_line):
                        line = inhandle.readline()
                count = int(line.split()[0])
                row['clipped_mapped_read_count'] = str(count)

            # filtered read count
            fname = os.path.join(DATAFOLDER["mapping_stats"], sample + ".filtered.bam.bamstats.txt")
            with open(fname, "r") as inhandle:
                for _ in range(target_line):
                    line = inhandle.readline()
            count = int(line.split()[0])
            row['filtered_mapped_read_count'] = str(count)
            
            # used read count
            count = int(line.split()[0])
            frac = int(row['filtered_mapped_read_count']) / int(row['raw_read_count'])
            row['survived_reads'] = format_percent(frac)

            # genome cov
            fname = os.path.join(DATAFOLDER["mapping_stats"], sample + ".coverage.tsv")
            with open(fname, "r") as inhandle:
                cov = [int(line.strip().split("\t")[2]) for line in inhandle]
            
            glen = len(cov)
            abs_cov = len([x for x in cov if x >= CNS_MINCOV])
            row["covered_genome_fraction_bp"] = str(abs_cov)
            row["covered_genome_fraction_%"] = format_percent(abs_cov / glen)
            row["avg_sequencing_depth"] = str(int(sum(cov) / glen))
            
            custom_covs = [50, 100, 200, 500]
            for c in custom_covs:
                cov_c = [x for x in cov if x >= c]
                abs_cov_c = len(cov_c)
                # You might want a different calculation for average sequencing depth here.
                row[f"{c}x_genome_fraction_bp"] = str(abs_cov_c)
                row[f"{c}x_genome_fraction_%"] = format_percent(abs_cov_c / glen)
                # Example: average depth over positions that meet the threshold
                row[f"{c}x_avg_sequencing_depth"] = str(int(sum(cov_c) / abs_cov_c)) if abs_cov_c > 0 else "0"

            rows.append(row)

        # Write CSV file once after processing all samples.
        with open(output[0], "w", newline="") as handle:
            # Use keys from the first row as column headers
            cols = list(rows[0].keys())
            writer = csv.DictWriter(handle, fieldnames=cols, delimiter='\t')
            writer.writeheader()  
            writer.writerows(rows)

rule create_rmd:
    input:
        summary=os.path.join(PROJFOLDER, "summary.csv"),
        rmd=os.path.join(os.path.dirname(workflow.snakefile), "..", "report.Rmd")
    output:
        html=os.path.join(PROJFOLDER, "summary.html")
    params:
        proj_folder=config.get("proj_folder", "."),
        list_folder=config.get("list_folder", "."),
        run_name=config.get("run_name", "none"),
        version=config.get("version", "none")
    conda:
        "../envs/r.yaml"
    shell:
        """
        Rscript -e "rmarkdown::render('{input.rmd}', output_file='{output.html}', params=list(
            summary_file='{input.summary}',
            proj_folder='{params.proj_folder}',
            list_folder='{params.list_folder}',
            run_name='{params.run_name}',
            version='{params.version}'))"
        """
