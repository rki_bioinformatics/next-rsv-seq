rule callVariants:
	input:
		pileup = os.path.join(DATAFOLDER["pileup"], "{sample}.pileup.gz"),
		reference = os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta")
	output:
		tsv = os.path.join(DATAFOLDER["variant_calling"], "{sample}.tsv")
	params:
		base_minqual = BASE_MINQUAL,
		var_minfreq = VAR_MINFREQ,
		cns_mincov = CNS_MINCOV,
		prefix = os.path.join(DATAFOLDER["variant_calling"], "{sample}"),
		gff = "-g " + os.path.join(DATAFOLDER["references"], "ref_for_{sample}.gff") 
	log:
		os.path.join(DATAFOLDER["logs"], "variant_calling", "{sample}.variantcall.log")
	conda:
		"../envs/ivar.yaml"
	threads: 1
	shell:
		r"""
			zcat {input.pileup} | ivar variants -p {params.prefix} -q {params.base_minqual} -t {params.var_minfreq} -m {params.cns_mincov} -r {input.reference} {params.gff} > {log} 2>&1
		"""
