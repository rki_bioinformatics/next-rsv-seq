# get sequence depth in single base resolution, an aggregation has to be performed at plotting level
# this data set can also be used to get the amount of bases with >0 sequence depth (genome coverage)

rule getCoverage:
	input:
		os.path.join(DATAFOLDER["mapping"], "{sample}.filtered.bam")
	output:
		os.path.join(DATAFOLDER["mapping_stats"], "{sample}.coverage.tsv")
	log:
		os.path.join(DATAFOLDER["logs"], "mapping_stats", "{sample}.coverage.log")
	conda:
		"../envs/bedtools.yaml"
	shell:
		r"""
			(bedtools genomecov -ibam {input} -d 1> {output}) 2> {log}
		"""
