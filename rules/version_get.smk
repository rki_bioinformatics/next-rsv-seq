rule getVersion:
    output:
        os.path.join(PROJFOLDER, "pipeline.version")
    params:
        sd = workflow.basedir
    log:
        os.path.join(DATAFOLDER["logs"], "version", "git.repository.version.log")
    shell:
        r"""
            cd {params.sd};
            ./rsvpipe.py --version 1> {output} 2> {log}
        """
