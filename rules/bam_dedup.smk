rule dedupReads:
	input:
		bam = os.path.join(DATAFOLDER["mapping"], "{sample}.sorted.bam")
	output:
		metric = os.path.join(DATAFOLDER["mapping_stats"], "{sample}.dedup.txt"),
		dedupped_bam = temp(os.path.join(DATAFOLDER["mapping"], "{sample}.dedupped.bam"))
	log:
		os.path.join(DATAFOLDER["logs"], "dedup", "{sample}.dedup.log")
	conda:
		"../envs/picard.yaml"
	threads: 1
	shell:
		r"""
			picard MarkDuplicates INPUT={input.bam} OUTPUT={output.dedupped_bam} METRICS_FILE={output.metric} REMOVE_DUPLICATES=true &>> {log};
    """
