rule runPileup:
	input:
		bam = os.path.join(DATAFOLDER["mapping"], "{sample}.filtered.bam"),
		reference = os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta"),
		ref_index = os.path.join(DATAFOLDER["references"], "ref_for_{sample}.fasta") + SAMTOOLS_INDEX_EXT
	output:
		pileup = os.path.join(DATAFOLDER["pileup"], "{sample}.pileup.gz")
	log:
		os.path.join(DATAFOLDER["logs"], "pileup", "{sample}.pileup.log")
	conda:
		"../envs/ivar.yaml"
	params:
		base_minqual = BASE_MINQUAL,
		algn_minqual = ALGN_MINQUAL
	threads: 1
	shell:
		r"""
			samtools mpileup -aa -d 0 -A -Q {params.base_minqual} -q {params.algn_minqual} -f {input.reference} {input.bam} | gzip > {output.pileup}  2> {log}
		"""
