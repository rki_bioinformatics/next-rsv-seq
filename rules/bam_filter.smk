def input_filterBam(wildcards):
    if PRIMER:
        return {
            "bam": os.path.join(DATAFOLDER["mapping"], f"{wildcards.sample}.clipped.bam"), 
            "bai": os.path.join(DATAFOLDER["mapping"], f"{wildcards.sample}.clipped.bam.bai")
        }
    elif READ_DEDUP:
        return {
            "bam": os.path.join(DATAFOLDER["mapping"], f"{wildcards.sample}.dedupped.bam"), 
            "bai": os.path.join(DATAFOLDER["mapping"], f"{wildcards.sample}.dedupped.bam.bai")
        }
    else:
        return {
            "bam": os.path.join(DATAFOLDER["mapping"], f"{wildcards.sample}.sorted.bam"), 
            "bai": os.path.join(DATAFOLDER["mapping"], f"{wildcards.sample}.sorted.bam.bai")
        }


rule filterBam:
	input:
		unpack(input_filterBam)
	output:
		temp(os.path.join(DATAFOLDER["mapping"], "{sample}.removed_clips.bam")),
		os.path.join(DATAFOLDER["mapping"], "{sample}.filtered.bam")
	log:
		os.path.join(DATAFOLDER["logs"], "mapping", "{sample}.filter.log")
	conda:
		"../envs/ngsutils.yaml"
	params:
		bq = ALGN_MINQUAL
	shell:
		r"""
			bamutils removeclipping {input.bam} {output[0]}
			samtools view -b -h -F 0x04 -q {params.bq} {output[0]} | samtools sort > {output[1]}
		"""
