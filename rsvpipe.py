#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#author: Stephan Fuchs (Robert Koch Institute, MF-1, fuchss@rki.de)

import os
import argparse
import sys
import yaml
import glob
import pandas as pd
from pathlib import Path
import subprocess
from __version__ import VERSION

default_regex = "([^" + os.path.sep + "]+)_R[12]_.+"

def parse_args():
	parser = argparse.ArgumentParser(prog="rsvpipe.py", description="an automated pipeline for reference-based RSV genome assembly")

	# data input
	group_input = parser.add_argument_group("DATA INPUT")
	group_input.add_argument('-f', '--file', metavar="FILE", help="fastq files with paired-end read data (forward and reverse reads are expected in individual files)", type=str, nargs='+', default=None)
	group_input.add_argument('-d', '--dir', metavar="DIR", help="directory that contains fastq files with paired-end read data", type=str, default=None)
	group_input.add_argument('-c', '--config', metavar="FILE", help="use existing config file (no other option allowed)", type=str, default=None)
	group_input.add_argument('-t', '--tag', metavar="STR", help="file name tag labelling forward and reverse read files (default: _R1_ and _R2_ )", type=str, nargs="+", default=['_R1_', '_R2_'])
	group_input.add_argument('-r', '--recursive', help="include files in subfolders of the directory ()", action="store_true")
	group_input.add_argument('-e', '--ext', metavar="STR", help="allowed file extensions (only effective when --dir is used; per default: .fq  .fastq  .fq.gz .fastq.gz )", type=str, nargs="+", default=['.fq', '.fastq', '.fq.gz', '.fastq.gz'])

	# reference selection
	group_reference = parser.add_argument_group("REFERENCE SELECTION")
	group_reference.add_argument('--ref', metavar="FILE", help="fasta file with reference sequence (default: auto selection)", type=str, default=None)
	group_reference.add_argument('--gff', metavar="FILE", help="GFF file of reference annotation (default: auto selection)", type=str, default=None)
	group_reference.add_argument('--ref-map', metavar="FILE", help="provide a file containing reference data-set mapping information, see README for more information (default: auto selection)", type=str, default=None)
	group_reference.add_argument('--fallback-ref', metavar="STR", help="reference to use when automatic selection fails (default: KM5175731.1)", choices=get_built_in_fastas(), default="KM5175731")

	# read trimming and filtering
	group_trimming = parser.add_argument_group("RAW READ QC")
	group_trimming.add_argument('-p', '--phred', metavar="INT", help="minimal averaged PHRED (default: 20)", type=int, default=20)
	group_trimming.add_argument('-l', '--length', metavar="INT", help="minimal read length (default: 50)", type=int, default=50)
	group_trimming.add_argument('-a', '--adapter', metavar="FILE", help="fasta file with adapter sequences to clip (default: no clipping)", type=str, default=None)
	group_trimming.add_argument('-k', '--krakendb', metavar="DIR", help="kraken2 database for taxonomic read filtering (default: no filtering)", type=str, default=None)
	group_trimming.add_argument('-x', '--taxid', metavar="STR", help="target taxonomic identifier (default: 12814)", type=str, default="12814")
	group_trimming.add_argument('-m', '--primer', metavar="FILE", help="bed file with information about amplicon primers to be clipped (default: no clipping)", type=str, default=None)
	group_trimming.add_argument('-u', '--dedup', help="remove read duplicates (default: no deduplication)", action="store_true")

	# read mapping
	group_mapping = parser.add_argument_group("READ MAPPING")
	group_mapping.add_argument('--mq', metavar="INT", help="minimum mapping quality of alignments to keep (default: 15)", type=int, default=15)

	# base calling
	group_calling = parser.add_argument_group("VARIANT & CONSENSUS CALLING")
	group_calling.add_argument('--dd', metavar="INT", help="mask sites with less coverage (default: 20)", type=int, default=10)
	group_calling.add_argument('--dr', metavar="INT", help="do not consider bases supported by less reads (default: 10)", type=int, default=10)
	group_calling.add_argument('--dq', metavar="INT", help="do not consider bases with less base call quality (default: 20)", type=int, default=20)
	group_calling.add_argument('--df', metavar="FLOAT", help="use most abundant bases to reach minimal frequency of supporting reads for consensus calling, only effective if--cns ivar (default: 0)", type=float, default=0)
	group_calling.add_argument('--dc', metavar="FLOAT", help="do not consider bases that have a lower relative proportion for consensus calling, only effective if--cns custom (default: 0.1)", type=float, default=0.1)
	group_calling.add_argument('--dp', metavar="FLOAT", help="do not consider snv with higher p-values for consensus calling, only effective if--cns custom (default: 0.05)", type=float, default=0.05)
	group_calling.add_argument('--cns', metavar="str", help="consensus calling algorithm to use (default: ivar)", choices=['ivar', 'custom'], default='ivar')
 
 
  # alert & report
	group_report = parser.add_argument_group("Report & Alerts")
	group_report.add_argument('--re', metavar="FLOAT", help="report the predicted effects of mutations occurring at or above the specified frequency (default: 0.5)", type=float, default="0.5")
	group_report.add_argument('--al', metavar="FLOAT", help="alert predefined mutations occurring at or above the specified frequency (default: 0.05)", type=float, default="0.05")  
	group_report.add_argument('--no_alert', help="deactivate variant annotation and alert sustem", action="store_true")
	group_report.add_argument('-o', '--out', metavar="STR", help="output directory (default: current working directory)", type=str, default="./results")
  

	# general
	group_out = parser.add_argument_group("GENERAL")
	group_out.add_argument('--show-refs', help="show all built-in references and exit.", action="store_true")
	group_out.add_argument('--threads', metavar="INT", help="number of threads to use (default: 20)", type=int, default=20)
	group_out.add_argument('--keep-temp', help="do not delete temporary files", action="store_true")
	group_out.add_argument('--mamba', help="use mamba instead of conda", action="store_true")
	group_out.add_argument('--rerun-incomplete', help="re-run an incompleted analysis", action="store_true")
	group_out.add_argument('--unlock', help="unlock directory", action="store_true")
	group_out.add_argument('-v', '--version', action='version', version='%(prog)s ' + VERSION)

	return parser.parse_args()

def get_built_in_fastas():
    # Get the directory of the script
    dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "data", "references")

    # Search for all fasta files in the script directory
    fasta_files = glob.glob(os.path.join(dir, "*.fasta"))

    # Get basenames without extensions
    basenames = [os.path.splitext(os.path.basename(file))[0] for file in fasta_files]

	# check if mate gff file
    for fname in basenames:
        fpath = os.path.join(dir, fname + ".gff")
        if not os.path.isfile(fpath):
            sys.exit("built-in reference data error: no gff file for fname.")

    return basenames

def check_inputfile(fname):
	if not os.path.isfile(fname):
		sys.exit("input error: data file does not exist (" + fname + ").")

def process_reflist(fname):
	df = pd.read_csv(fname, sep="\t")
	if 'SAMPLE' not in df.columns:
		sys.exit("input error: The required columns 'SAMPLE' is not present in the given reference listing file.")
	
	df.set_index('SAMPLE', inplace=True)

	if "GFF" not in df.columns or "FASTA" not in df.columns:
		sys.exit("input error: The required columns 'GFF' and 'FASTA' are not present in the given reference listing file.")

	if df.index.duplicated().any():
		sys.exit("input error: The 'SAMPLE'' columns contains non-unique sample names.")

	df['FASTA_basename'] = df['FASTA'].apply(lambda x: os.path.splitext(os.path.basename(x))[0])
	if df['FASTA_basename'].duplicated().any():
		sys.exit(f"input error: FASTA file names provided in the reference listing file have to be unique (file extensions are ignored).")

	ref_files = df[["GFF", "FASTA"]].to_dict(orient="index")

	for fnames in ref_files.values():
		if not os.path.isfile(fnames[0]) or not os.path.isfile(fnames[1]):
			sys.exit(f"input error: non existing file(s) in the reference listing file.")	
	return ref_files

def show_references():
	print("The following references are available:")
	for fname in get_built_in_fastas():
		print(os.path.splitext(fname)[0])

def main():
	config = {}
	samples = {}
	args = parse_args()

	# show refs
	if args.show_refs:
		show_references()
		sys.exit()


	# checking data input arguments
	i = sum(x is not None for x in [args.file, args.dir, args.config])
	if i > 1:
		sys.exit("input error: options --file, --dir, and --config are mutually exclusive.")
	elif i == 0:
		sys.exit("input error: no files, directory or existing config file has been set.")

	if len(args.tag) != 2:
		sys.exit("input error: there must be exactly two data file name tags defined.")

	# checking fastq files
	if args.file:
		fnames = args.file
	elif args.dir:
		if not os.path.isdir(args.dir):
			sys.exit("input error: data directory does not exist (" + args.dir + ").")
		p = Path(args.dir).rglob('*') if args.recursive else Path(args.dir).glob('*')
		fnames = [str(x.resolve()) for x in p if x.name.endswith(tuple(args.ext)) ]
	else:
		with open(args.config, "r") as handle:
			if not os.path.isfile(args.config):
				sys.exit("input error: config file does not exist (" + os.path.basename(args.config) + ").")
			try:
				config = yaml.safe_load(handle)
			except:
				sys.exit("input error: config file cannot processed.")

			if not "samples" in config:
				sys.exit("input error: missing samples definition in contig file")

			with open(config['samples'], "r") as handle:
				samples = yaml.safe_load(handle)
			
			fnames = []
			for sample_id in samples:
				fnames += [samples[sample_id]['fwd'], samples[sample_id]['rev']]
			
	for fname in fnames:
		check_inputfile(fname)

	if len(fnames)%2 != 0:
		sys.exit("input error: paired-end data expected (even number of input files mandatory)")
	elif len(fnames) == 0:
		sys.exit("input error: no fastq data file found")


	if args.ref_map:
		ref_files = process_reflist(args.ref_map)

	if args.dir or args.file:
		# reference
		if args.ref and args.ref_map:
			sys.exit("Input error: --ref and args.ref-map are mutually exclusive.")
		if args.gff and args.ref_map:
			sys.exit("Input error: --gff and args.ref-map are mutually exclusive.")

		if args.ref_map and not os.path.isfile(args.ref_map):
			sys.exit("Input error: no valid file submitted for --ref-map")

		if args.ref and not os.path.isfile(args.ref):
			sys.exit("Input error: value of --ref is not a valid file.")

		if args.gff and not args.ref:
			sys.exit("Input error: --gff needs --ref set.")
		
		if args.ref and not args.gff:
			sys.exit("Input error: --ref needs --gff set.")

		if args.gff and not os.path.isfile(args.gff):
			sys.exit("Input error: no valid file submitted for --gff")

		# samples
		fnames.sort()
		args.ext.sort(key = len, reverse=True)
		if len(fnames) < 2:
			sys.exit("input error: at least 2 fastq files have to be provided.")
		for i in range(0, len(fnames), 2):
			f1 = os.path.basename(fnames[i])
			f2 = os.path.basename(fnames[i+1])
			if f1.replace(args.tag[0], args.tag[1]) != f2 or f1 != f2.replace(args.tag[1], args.tag[0]):
				sys.exit("input error: file name pairing failed for data file pair " + os.path.basename(fnames[i]) + " and " + os.path.basename(fnames[i+1]))
			sample_id = f1.replace(args.tag[0], "_R_")
			for ext in args.ext:
				if sample_id.endswith(ext):
					sample_id = sample_id[:-len(ext)]
					break
			if sample_id in samples:
				sys.exit("input error: sample id collision for different file pairs (id: " + sample_id + ").")
			samples[sample_id] = {}
			samples[sample_id]['altid'] = 'Sample_' + str(int(i/2))
			samples[sample_id]['fwd'] = fnames[i]
			samples[sample_id]['rev'] = fnames[i+1]
			if args.ref:
				samples[sample_id]['ref_fasta'] = args.ref
				samples[sample_id]['ref_gff'] = args.gff
			elif args.ref_map:
				if sample_id not in ref_files:
					sys.exit(f"input error: The sample '{sample_id}' not listed in reference listing file")
				samples[sample_id]['ref_fasta'] = ref_files[sample_id]['FASTA']
				samples[sample_id]['ref_gff'] = ref_files[sample_id]['GFF']
			else:
				samples[sample_id]['ref_fasta'] = None
				samples[sample_id]['ref_gff'] = None
		
		# trimming
		if args.phred < 0:
			sys.exit("Input error: value of --phred cannot be smaller than 0.")

		if args.length < 0:
			sys.exit("Input error: value of --lengths cannot be smaller than 0.")

		if args.adapter and not os.path.isfile(args.adapter):
			sys.exit("Input error: value of --adapter is not a valid file.")

		if args.krakendb and not os.path.isdir(args.krakendb):
			sys.exit("Input error: value of --krakendb is not a valid directory.")

		if args.primer and not os.path.isfile(args.primer):
			sys.exit("Input error: value of --primer is not a valid file.")

		# variant calling
		if args.mq < 0:
			sys.exit("Input error: value of --mq cannot be smaller than 0.")

		if args.dd < 0:
			sys.exit("Input error: value of --dd cannot be smaller than 0.")

		if args.dr < 0:
			sys.exit("Input error: value of --dd cannot be smaller than 0.")

		if args.dq < 0:
			sys.exit("Input error: value of --dq cannot be smaller than 0.")

		if args.df < 0 or args.df > 1:
			sys.exit("Input error: value of --df has to be between 0 and 1.")

		if args.dc < 0 or args.dc > 1:
			sys.exit("Input error: value of --dc has to be between 0 and 1.")

		if args.dp < 0 or args.dp > 1:
			sys.exit("Input error: value of --dp has to be between 0 and 1.")
      
		if args.re < 0 or args.re > 1:
			sys.exit("Input error: value of --re has to be between 0 and 1.")      
      
		if args.al < 0 or args.al > 1:
			sys.exit("Input error: value of --al has to be between 0 and 1.") 
      
		# assigning config keys
		config['output'] = os.path.abspath(args.out)
		config['samples'] = os.path.join(args.out, "samples.yaml")
		config['fallback_ref'] = args.fallback_ref if not args.ref and not args.ref_map else None
		config['kma'] = True if not args.ref and not args.ref_map else False
		config['krakenDb'] = os.path.abspath(args.krakendb) if args.krakendb else None
		config['krakenTaxID'] = args.taxid
		config['adapter'] = os.path.abspath(args.adapter) if args.adapter else None
		config['primer'] = os.path.abspath(args.primer) if args.primer else None
		config['read_dedup'] = args.dedup
		config['algn_minqual'] = args.mq
		config['base_minqual'] = args.dq
		config['var_minfreq'] = 0
		config['cns_minfreq_ivar'] = args.df
		config['cns_minfreq_custom'] = args.dc
		config['cns_mincount'] = args.dr
		config['cns_mincov'] = args.dd
		config['cns_maxpval'] = args.dp
		config['read_minqual'] = args.phred
		config['read_minlen'] = args.length
		config['cns_algo'] = args.cns
		config['moc_treshold'] = args.re
		config['alert_treshold'] = args.al
		config['no_alert'] = args.no_alert
		configfile = os.path.join(config['output'], "config.yaml")
		os.makedirs(os.path.join(config['output']), exist_ok=True)

		with open(configfile, "w") as handle:
			yaml.dump(config, handle)

		with open(config['samples'], "w") as handle:
			yaml.dump(samples, handle)
	else:
		configfile = args.config

	#run snakemake
	path = os.path.dirname(os.path.abspath(__file__))
	snakefile = os.path.join(path, "rsvpipe.snake")
	cmd = ["snakemake", "-s", snakefile, "--configfile", configfile, "--cores",  str(args.threads), "--use-conda" , "--conda-frontend", "mamba"]
	if args.keep_temp:
		cmd += ["--notemp"]
	if args.rerun_incomplete:
		cmd += ["--rerun-incomplete"]
	if args.unlock:
		cmd += ["--unlock"]		
	if args.mamba:
		cmd += ["--conda-frontend", "mamba"]
	subprocess.Popen(cmd).wait()

if __name__ == "__main__":
    main()
